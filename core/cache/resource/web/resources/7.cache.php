<?php  return array (
  'resourceClass' => 'modDocument',
  'resource' => 
  array (
    'id' => 7,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'Newsroom',
    'longtitle' => '',
    'description' => '',
    'alias' => 'newsroom',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 0,
    'isfolder' => 0,
    'introtext' => '',
    'content' => '',
    'richtext' => 1,
    'template' => 8,
    'menuindex' => 5,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 2,
    'createdon' => 1430088235,
    'editedby' => 2,
    'editedon' => 1430103681,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1430088180,
    'publishedby' => 2,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'newsroom',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'background-image' => 
    array (
      0 => 'background-image',
      1 => 'newsroom_header.jpg',
      2 => 'default',
      3 => NULL,
      4 => 'image',
    ),
    'articles' => 
    array (
      0 => 'articles',
      1 => '[{"MIGX_id":"1","article-title":"Genesis Report February 26th, 2015","article-image":"circle_1.jpg","article-teaser":"<p><span>I recently attended the Fuel Ethanol Workshop in Indianapolis where I was surrounded by industry leaders in renewable fuels markets from around the US and the world. The main topic headlining the conference was second generation ethanol, principally cellulosic ethanol made from corn stover. The general consensus was that cellulosic ethanol was the next step in renewable fuels and that we were at the gates of unlocking that technology. The concept of cellulosic ethanol has been around for a while. One presenter found a copy of a magazine article declaring that cellulosic ethanol would be in an advanced state of development within the next 5 years. Unfortunately, that article was written in 1982. Here we are in 2014, and the industry is still dipping their toes in the cold, unchartered waters of second generation ethanol.</span></p>","article-content":"<p>There are currently three companies that have built cellulosic ethanol plants, and they all agree on one thing. While they think cellulosic ethanol has a bright future, the reality is it is still a long ways off from becoming perfected to the point that it is reproducible and sustainable. There is no 5 step simple process that can be written on a wall. Like any new venture, many of the problems are yet to be experienced.</p>\\n<p>Project Liberty is one of the cellulosic pioneers being developed by POET DSM. POET is adding on to their current ethanol plant in Emmetsburg, Iowa with a 20 million gallon biomass plant. They have already sourced 100,000 tons of feedstock, primarily corn stover, to feed their plant. One of the major hurdles to they foresee in the supply chain is the harvest window is very short. They have only 4 to 6 weeks to collect all of the corn stover possible after harvest. This is much different from corn where it can be sourced almost year round if needed. Large volumes of corn stover must be baled and stored in a short amount of time. This leads to other risks such as fire or storage damage that they are still working on the best methods to reduce or eliminate. Participants still couldn’t agree on the best way to bale the corn stover. Whoever discovers the best and most efficient way to harvest, bundle and ship the corn stover will unlock the keys to a successful cellulosic ethanol program.</p>\\n<p>Additionally, they are still building relationships with farmers since it requires change on their part. Dupont expects to have up to 300 farmer clients in a 30 mile radius surrounding their plant in Nevada, Iowa. Depending upon whether the farmer wants to custom hire his corn stover harvest or do it himself, it may require some investment on their part. Equipment manufacturers such as Ag Co and New Holland were busy marketing their new balers. They have also developed corn heads that windrow the crop residue. Some combines can blow the crop residue directly into the baler as they harvest the corn and bale the corn stover at the same time, reducing the process to a single pass.</p>\\n<p>One of the biggest concerns to removing corn stover is the amount of nutrient removal taking place. Dupont estimates that with a 200 bushel per acre yield, they can remove 2 tons of dry matter per acre. This equates to roughly 34 lbs of nitrogen, 8 lbs of phosphorus, and 68 lbs of potassium per acre. This is by no means a one size fits all. Some areas that are prone to erosion should not have any corn residue removed. Corn stover removal will need to be managed on a site specific basis. Advocates for corn stover removal estimate a yield advantage of 5.2 bushels per acre due to the removal of excessive residue. POET DSM believes that a farmer could expect an additional U$46 per acre revenue increase from a corn stover program depending upon whether the farmer uses a corn-soybean or corn on corn rotation. That is already taking into account any nutrient supplementation that may have to take place in the loss of crop residue as well as savings in tillage. That is an additional U$96,000 on a 2000 acre operation.</p>\\n<p>Dupont believes that someday there could be the potential for up to 300 plants in the US producing cellulosic ethanol requiring capital investments of up to U$60 billion. This will have major ramifications for farmers and will create business opportunities in the format of custom harvesters, stover commodity trading, transportation and logistics. Dupont estimates that for a 30 million gallon plant with a 30 mile radius, there should be approximately 815,000 supply acres of corn stover of which they only need 190,000 acres or 23% of this to fuel a plant that size. Harvesting two tons per acre will produce an estimated 700,000 bales of corn stover required to fuel the plant. The implications are exciting to say the least and warrant further investigation.</p>"},{"MIGX_id":"2","article-title":"In the Doldrums, Brazil Looks to U.S.","article-image":"circle_2.jpg","article-teaser":"<p><a href=\\"http://www.wsj.com/articles/in-the-doldrums-brazil-looks-to-u-s-1425866454\\">http://www.wsj.com/articles/in-the-doldrums-brazil-looks-to-u-s-1425866454</a><span> </span><a href=\\"newsroom.html\\">learn more</a></p>","article-content":""},{"MIGX_id":"3","article-title":"Genesis Report February 26th, 2015","article-image":"circle_3.jpg","article-teaser":"<p><span>I recently attended the National Ethanol Conference in Grapevine, Texas where one of the topics discussed was the road ahead for higher blends of ethanol.</span></p>","article-content":"<p>E15 has begun to slowly etch out a market for itself since first being introduced in a single fuel station in Kansas only three years ago. Today there are already 100 stations in 16 states selling E15. There will soon be more as 29 states have already approved it for use. Despite what Big Oil advocates have to say, E15 has proven itself to be safe and popular alternative to regular gasoline. The Renewable Fuels Association reports that more than 100 million miles have been traveled without a single complaint or incident. E15 offers consumers a higher octane at a lower price.</p>\\n<p>The Environmental Protection Agency allows for the use of E15 fuel in all vehicles built since 2001, which account for 85% of transportation vehicles. What is more promising is that the automakers themselves have begun to approve the use of E15 in most of their vehicles. 70% of all new vehicles produced in 2015 will be formally approved for E15 use.</p>\\n<p>From 2007 to 2014, there has been a cumulative increase of 255% of Flex Fuel Vehicles sold in the United States. Even more promising is that these numbers are expected to grow from the 16 million Flex Fuel Vehicles that exist in today’s market to over 25 million by 2023. Assuming there really are 25 million Flex Fuel Vehicles registered by 2023, and only half of those vehicles used E85, that alone would create demand for nearly 16 billion gallons of ethanol per year. If only 5% of those same vehicles used E85, that would create demand for 1.6 billion gallons of ethanol.</p>\\n<p>Americans like to think they are the tip of the spear when it comes to technology and innovation, but when it comes to adopting ethanol we are far from being the leader. Mid-level ethanol blends have been used in other countries for over three decades. Brazil has been using E25 blends since the late 70’s, and their legislature is expected to increase that to E27 very soon. What comes to mind when you think of Thailand? Somehow being a pioneer of alternative energy is not it. And yet over half of all fuel stations in Thailand sell E20 as well as half of all vehicles in Thailand are approved for E20 use. In 2014, E20 was 7% cheaper than E10 and 27% cheaper than regular gasoline.</p>\\n<p>It was at the National Ethanol Conference in Grapevine, Texas where I learned that in 2013 there was nearly 153,000 fueling stations in the United States. 80% of those are convenience stores and 13% are connected to large-scale supermarkets. The remaining amounts are sold at locations without stores, typically small-volume sites like marinas. According to the Renewable Fuels Association, currently 2% of fuel stations in America offer E85. The RFA also says that that number has been increasing steadily in the last ten years, growing at 14% per year. However, since we started near zero ten years ago, that doesn’t say much for total volume. If we were to continue at the same rate of growth, in another ten years we would have closer to 11,000 E85 stations as opposed to the 3300 that we have now. Assuming the number of fuel stations stay the same, E85 would be represented by only 7% of American’s fueling stations by the year 2023.</p>\\n<p>58% of fuel station owners only own one store. While 17% of fuel stations owners, own over 500 stores or more. Approximately 50% of all fuel stations sell branded fuel, meaning they would have some sort of long term agreement with a fuel distributor or producer to carry one specific brand of fuel. This is why it can be a slower process for fueling stations to install higher ethanol blends, as they can only consider the option to do it once their contracts have expired. It is also why consumers need to talk to their local fuel station to advocate for increased ethanol blends once those branded fuel contracts expire.</p>\\n<p>Convenience stores averaged about 128,000 gallons sold per month in 2013. Large- scale grocery markets sell over twice that amount. The average profit per gallon is approximately 4 US cents. The average profit per store was US$55,000, but interestingly enough, while transportation fuel makes up 71% of sales, it only makes up 36% of profit. In other words, the fuel station owner may be more concerned about being stocked up on energy drinks than he is E30 or E85.</p>"},{"MIGX_id":"4","article-title":"Genesis Report July, 2014","article-image":"circle_4.jpg","article-teaser":"<p><span>As a final requisite for earning my Executive MBA from Walden University, I was required to meet my fellow classmates in a major emerging market for the purpose of broadening our understanding of a new and foreign economy with swelling prospects. We chose China. Shanghai has long been a hub of international trade attracting foreign investments from all over the world. Shanghai has become an anomaly among an otherwise largely undeveloped country. While almost one-fourth of China’s 1.3 billion population still lives on less than U$2 per day, most of the wealth is concentrated in Shanghai, a city twice the size of New York. In downtown Shanghai, I was never more than a few blocks away from a Starbucks. BMW’s and Mercedes Benz were as common as chop sticks. While citizens of Shanghai are accustomed to the luxury of western cultures, most of the Chinese that live in the country side have never seen a foreigner. While hiking in the Yellow Mountain region in the Anhui province, I received many smiles and looks of curiosity from my fellow Chinese hikers. Like other members in my graduate class, I received many requests from the Chinese to have their picture taken with me. Intrigued by their curiosity, I asked my local tour guide to explain this odd behavior. He explained that since many Chinese have still never seen a foreigner in person, especially one with a 6 foot 4 inch frame like mine, the popular thing to do is to take a picture with one so they can brag to their friends later, making up stories about how they met a big Hollywood movie star. As time goes on and income levels continue to rise in China, this cultural nuance will dissipate as the number of Chinese looking to travel abroad is estimated to double from its current level of 100 million people to 200 million people by the year 2020. One of their top choices for international travel...the United States.</span></p>","article-content":"<p>Our group visited the offices of JWT, a global advertising company founded in 1864, currently operating in 90 countries. They provided valuable insight into the Chinese psyche and way of life. If you are Chinese, you typically have the highest personal savings rate in the world at an excess of 30% of your annual income. That is five times what it is in the United States. This is partly due to the lack of government welfare programs as well as the higher cost of education. The Chinese are less impulsive and more pragmatic operating under the mindset that we don’t know what the future holds, and therefore it is important to save for a rainy day.</p>\\n<p>If you are Chinese and under the age of 30, your generation has been dubbed the little emperor. The reason for this is you grew up a lonely child, due to the one-child rule. Therefore, you were both spoiled by your parents and grandparents and given every opportunity affordable. Naturally, the parents hope that the child will become more successful than their elders. But this is not born out of selfless parental love, but rather in China the parents and grandparents, being of a more collective society, know they will rely on the success of their children. Therefore, the success of the child is just as important to the parents so they have someone to continue to take care of them when they are older as there is no social security program as we know it. While the one-child rule has reduced an estimated 400 million Chinese from being born by the year 2011, the negative consequence of this is that there are less children to take care of their parents and grandparents. Parents cannot afford, literally, to let their children fail. That is why they typically spare no expense in regards to their education. It is now possible to find what is called, “Baby MBA’s”, designed for toddlers to give them an edge in business as soon as possible. This pressure to succeed can weigh heavily on young Chinese students. This creates a stressful and exhausting environment to both the parents and students as everyone, including the grandparents are involved in raising the child. Our local guide already had one child. He was considering having a second, which could be made possible by paying a steep fine to the government. He was hesitant in having a second child, not because of the fine, but he wasn’t sure if the six of them working together could handle the pressure of raising a second child.</p>\\n<p>If you are Chinese, you are strongly encouraged to learn English. It is estimated that there are as many people learning in English in China, as there are Americans living in the United States.</p>\\n<p>If you are Chinese, product popularity is the number one driver of choice. Chinese want to look good more than feel good. Starbucks is very popular right now and can be found every few blocks in Shanghai. I am told that because Starbucks is both expensive and foreign, this means it is cool. So cool, in fact that many Starbucks customers in Shanghai buy a cup of coffee and don’t even drink it. Rather, they just want to be seen with it. Chinese consumers go out of their way to have the best clothes, best watch, best car, but may not have anything in their home since entertaining guests is a rare occurrence. I will talk more about China’s economy in tomorrow’s report.</p>"},{"MIGX_id":"5","article-title":"Genesis Report June, 2014","article-image":"circle_5.jpg","article-teaser":"<p><span>I recently attended the Fuel Ethanol Workshop in Indianapolis where I was surrounded by industry leaders in renewable fuels markets from around the US and the world. The main topic headlining the conference was second generation ethanol, principally cellulosic ethanol made from corn stover. The general consensus was that cellulosic ethanol was the next step in renewable fuels and that we were at the gates of unlocking that technology. The concept of cellulosic ethanol has been around for a while. One presenter found a copy of a magazine article declaring that cellulosic ethanol would be in an advanced state of development within the next 5 years. Unfortunately, that article was written in 1982. Here we are in 2014, and the industry is still dipping their toes in the cold, unchartered waters of second generation ethanol.</span></p>","article-content":"<p>There are currently three companies that have built cellulosic ethanol plants, and they all agree on one thing. While they think cellulosic ethanol has a bright future, the reality is it is still a long ways off from becoming perfected to the point that it is reproducible and sustainable. There is no 5 step simple process that can be written on a wall. Like any new venture, many of the problems are yet to be experienced.</p>\\n<p>Project Liberty is one of the cellulosic pioneers being developed by POET DSM. POET is adding on to their current ethanol plant in Emmetsburg, Iowa with a 20 million gallon biomass plant. They have already sourced 100,000 tons of feedstock, primarily corn stover, to feed their plant. One of the major hurdles to they foresee in the supply chain is the harvest window is very short. They have only 4 to 6 weeks to collect all of the corn stover possible after harvest. This is much different from corn where it can be sourced almost year round if needed. Large volumes of corn stover must be baled and stored in a short amount of time. This leads to other risks such as fire or storage damage that they are still working on the best methods to reduce or eliminate. Participants still couldn’t agree on the best way to bale the corn stover. Whoever discovers the best and most efficient way to harvest, bundle and ship the corn stover will unlock the keys to a successful cellulosic ethanol program.</p>\\n<p>Additionally, they are still building relationships with farmers since it requires change on their part. Dupont expects to have up to 300 farmer clients in a 30 mile radius surrounding their plant in Nevada, Iowa. Depending upon whether the farmer wants to custom hire his corn stover harvest or do it himself, it may require some investment on their part. Equipment manufacturers such as Ag Co and New Holland were busy marketing their new balers. They have also developed corn heads that windrow the crop residue. Some combines can blow the crop residue directly into the baler as they harvest the corn and bale the corn stover at the same time, reducing the process to a single pass.</p>\\n<p>One of the biggest concerns to removing corn stover is the amount of nutrient removal taking place. Dupont estimates that with a 200 bushel per acre yield, they can remove 2 tons of dry matter per acre. This equates to roughly 34 lbs of nitrogen, 8 lbs of phosphorus, and 68 lbs of potassium per acre. This is by no means a one size fits all. Some areas that are prone to erosion should not have any corn residue removed. Corn stover removal will need to be managed on a site specific basis. Advocates for corn stover removal estimate a yield advantage of 5.2 bushels per acre due to the removal of excessive residue. POET DSM believes that a farmer could expect an additional U$46 per acre revenue increase from a corn stover program depending upon whether the farmer uses a corn-soybean or corn on corn rotation. That is already taking into account any nutrient supplementation that may have to take place in the loss of crop residue as well as savings in tillage. That is an additional U$96,000 on a 2000 acre operation.</p>\\n<p>Dupont believes that someday there could be the potential for up to 300 plants in the US producing cellulosic ethanol requiring capital investments of up to U$60 billion. This will have major ramifications for farmers and will create business opportunities in the format of custom harvesters, stover commodity trading, transportation and logistics. Dupont estimates that for a 30 million gallon plant with a 30 mile radius, there should be approximately 815,000 supply acres of corn stover of which they only need 190,000 acres or 23% of this to fuel a plant that size. Harvesting two tons per acre will produce an estimated 700,000 bales of corn stover required to fuel the plant. The implications are exciting to say the least and warrant further investigation.</p>\\n<p> </p>"},{"MIGX_id":"6","article-title":"Genesis Report 2005","article-image":"circle_6.jpg","article-teaser":"<p><span>Advocates of genetically engineered crops argue that the plants offer an environmentally friendly alternative to pesticides, which tend to pollute surface and groundwater and harm wildlife. The use of Bt varieties has dramatically reduced the amount of pesticide applied to cotton crops. But the effects of genetic engineering on pesticide use with more widely grown crops are less clear-cut. What might be the effect of these engineered plants on so-called non target organisms, the creatures that visit them? Concerns that crops with built-in insecticides might damage wildlife were inflamed in 1999 by the report of a study suggesting that Bt corn pollen harmed monarch butterfly caterpillars. Monarch caterpillars don’t feed on corn pollen, but they do feed on the leaves of milkweed plants, which often grow in and around cornfields. Entomologists at Cornell University showed that in the laboratory Bt corn pollen dusted onto milkweed leaves stunted or killed some of the monarch caterpillars that ate the leaves. For some environmental activist this was confirmation that genetically engineered crops were dangerous to wildlife. But follow-up studies in the field, reported last fall, indicate that pollen densities from Bt corn rarely reach damaging levels on milkweed, even when monarchs are feeding on plants within a cornfield. The chances of a caterpillar finding Bt pollen doses as high as those in the Cornell study are negligible”, Rick Hellmich, an entomologist with the Ag Research Service and one author of the follow-up report. “Butterflies are safer in a Bt cornfield than they are in a conventional cornfield, when they’re subjected to chemical pesticides that kill not just caterpillars but most insects in the field.” 90% comply with requirements to combat bugs from becoming resistant; farmers are encouraged to plant a moat of conventional crops around their engineered crops so that a resistant bug will mate with a non-resistant bug to slow the evolutionary process.</span></p>","article-content":"<p>In my Nobel lecture, Borlaug suggested we had until the year 2000 to tame the population monster, and then food shortages would take us under. Now I believe we have a little longer. The Green Revolution can make Africa productive. The breakup of the former Soviet Union has caused its grain output to plummet, but if the new republics recover economically, they could produce vast amounts of food. More fertilizer can make the favored lands of Latin America – especially Argentina and Brazil – more productive. The cerrado region of Brazil, a very large area long assumed to be infertile because of toxic soluble aluminum in the soil, bay become a breadbasket, because aluminum-resistant crop strains are being developed.” This last is an example of agricultural advances and environmental protection going hand in hand: in the past decade the deforestation rate in the Amazon rain forest has declined somewhat, partly because the cerrado now looks more attractive.</p>\\n<p>Who would you say has saved more lives in the history of mankind? Some would say it was Mother Theresa, who devoted her life to working with the poor and hungry. Maybe it was Alexander Fleming, who discovered the most effective life-saving drug in the world – penicillin. But have you ever heard of the name Norman Borlaug? Although he was born in Iowa, he spent a greater portion of his life in Third World countries teaching the techniques of high-yield agriculture. Due to Borlaug’s efforts around the world, his programs have resulted in a number of countries becoming either self-sufficient in production of cereals, that otherwise would not have due to massive population increases. This has resulted in the prevention of millions of people starving to death in developing countries. He has managed to grow crops in regions where Dust Bowl experiences like the one back in the 30’s are a regular fact of life.</p>\\n<p>The world’s grain output in 1950 of 692 million tons came from 1.7 billion acres of cropland. Compare that to 1992 output of 1.9 billion tons of grain output from 1.73 billion acres and that is a 170 % grain increase from one percent more land. Before we pat ourselves on the back, look at what we’re up against. In the year 1900, world population was roughly 1 billion people. In the year 2000, world population was about 6 billion people. And world population is projected to grow close to 10 billion by the year 2050. To bring the entire world’s consumption habits that year to a level comparable to that of the West, will require a 430 % increase from current food production.</p>\\n<p>Up until the Green Revolution spread to South America and then to Asia, the only way for developing farmers to keep up with population growth was to convert forests, jungles and deserts into farmland. More productive crop varieties developed during the Green Revolution allowed farmers to grow vastly more food on only slightly more land. High- yield agriculture holds back the rampant deforestation of wild areas. Borlaug helped transition India into a high-yield farming area that otherwise would have had to plough an additional 100 million acres of virgin land-an area the size of California.</p>\\n<p>“To feed a village for a day, give them fish. But to feed them for a lifetime, teach them how to fish.” Some in opposition to biotechnology would rather have an equal distribution of the food supply rather than teach others how to fish. While I’m not against it, I question its feasibility.</p>\\n<p>Another value to Borlaug’s work, statistics suggest that high-yield agriculture brakes population growth rather than accelerating it. “Development is the best defense” In subsistence agriculture children are viewed as manual labor, and thus large numbers are desired. In technical agriculture, knowledge becomes more important, and parents thus have fewer children in order to devote resources to their education. I don’t think this is limited to just development of agriculture, but also development of a working society with a stable government and economy.</p>"},{"MIGX_id":"7","article-title":"Genesis Report 2003","article-image":"circle_7.jpg","article-teaser":"<p><span>In Thomas Friedman’s book, “The Lexus and the Olive Tree”, he discusses the increasingly important role of multi-national companies, who engage in foreign direct investment. Companies like Nike, Ford, Coca- cola, and Microsoft continue to pour more money into more developing countries than ever before. Agriculture is not the only industry that faces tougher competition and squeezed profit-margins. The computer, textile, automobile and beverage industry all feel the pinch. The only difference between us and them is in how we respond. It is not just a choice that these large multi-national companies make to invest abroad, but a necessity. Friedman points out that “every big multi-national needs to try to sell globally in order to make up in volume for shrinking profit margins as well as produce globally so that it can do it the cheapest and most efficiently – in order to keep manufacturing costs down and remain competitive.” I believe the same can be said for agriculture.</span></p>","article-content":"<p>Brazil is one of those developing countries that multi-national firms seek out for their investment. According to a Brazilian consultant in Friedman’s book, “Multi-nationals are a necessary good. Latin America is still highly dependent on external capital, because domestic savings are simply not enough to sustain high economic growth. Therefore, they need direct foreign investment. These multi-national’s bring two very important things with them besides their money. The first of these are international standards for conducting business. After spending time working in Brazil, it is obvious that each country has its own business culture. I have seen many things take place here in Brazil that would be unheard of back home. Bribes and kickbacks abound in many developing countries and Brazil is no exception.</p>\\n<p>When a multi-national company wants to invest in a particular country, it has certain criteria that must be met. One of these being certain standards for business behavior. If these criteria are not met, that country risks losing that investment. An example of this is by a Brazilian based computer company called Vitech, which ironically is founded out of the state of Bahia – the same state Brazil Iowa Farms, LLC is to locate. The chairman of Vitech explains in Friedman’s book that he does not hesitate to let the Brazilian authorities know exactly what he needs if he is going to keep his computer company there, with all the jobs and technology transfers that come with it. The chairman goes on to say that the politicians in Brazil are used to having people come and sit at their throne, as they dole out goodies and power as they see fit. This idea has changed as those who bring in the capital, make the demands and dictate the rules.</p>\\n<p>The second thing that multi-national companies bring in, along with their capital is technology. This technology is as attractive as the capital to these countries because it not only can improve the standard of living, but also boosts the quality and competitiveness of that particular industry. A country may have the labor and the capital, but if it doesn’t have the technology, it cannot compete. Taiwan is a perfect example of a country that has greatly benefited from the technology that has been brought in from other countries. While other countries chose to impose trade tariffs on foreign investors, Taiwan held out open arms for those multi-national companies looking to invest in their country.</p>\\n<p>According to the World Bank, the share of total world output by local affiliated factories of multinational corporations has gone from 4.5 percent of world GDP in 1970 to double that amount today. Foreign direct investment is not only increasing in terms of dollar size, but the locations of investments are increasing as well. Foreign investments are no longer contained to a few emerging markets, but rather multiple developing nations.</p>\\n<p>Like other industries, agriculture feels the impact of globalization and world trade. I believe that the agriculture industry needs to follow suit with other industries if it is to remain the most efficient and competitive. No matter whether you are producing computers for Vitech, or producing grain for Brazil Iowa Farms, it is evident that to be successful in this day and age you must be willing to look to where the opportunity lies, regardless of boundaries.</p>"}]',
      2 => 'default',
      3 => NULL,
      4 => 'migx',
    ),
    '_content' => '<!doctype html>
<head>
<meta charset="UTF-8">
<title> Genesis |  </title>
<meta name="description" content="">
<link rel="icon" type="image/jpg" href="favicon.png">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta property="og:title" content=""/>
<meta property="og:image:type" content="image/jpeg" />
<meta property="og:image" content=""/>
<meta property="og:url" content="" />
<meta property="og:description" content=""/>
<link href="css/bootstrap.min.css" rel="stylesheet" />
<link href="css/main.css" rel="stylesheet" type="text/css" />
<script src="js/respond.min.js"></script>
<script src="js/modernizr.custom.js"></script>
<script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
<link href="css/normalize.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="css/animate.css">
<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
<![endif]-->
<script>

(function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,\'script\',\'//www.google-analytics.com/analytics.js\',\'ga\');

ga(\'create\', \'UA-43660403-1\', \'auto\');
ga(\'send\', \'pageview\');

$(function() {
  $(\'a[href*=#]:not([href=#])\').click(function() {
    if (location.pathname.replace(/^\\//,\'\') == this.pathname.replace(/^\\//,\'\') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $(\'[name=\' + this.hash.slice(1) +\']\');
      if (target.length) {
        $(\'html,body\').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});

</script> 
</head>

<body>

<div class="container-fluid">

    <!-- row 1: navigation -->
    <div class="row">
    <div id="nav">
      <div class="limiter">
        <div class="navbar logobar">
          <div class="headerimage">
            <a href="index.html"><img  src="images/header_logo.png" width="180"  alt=""/></a> 
          </div>
          <div class="header_text pull-right" >Your business partner for Brazilian expansion</div>
        </div>
      </div>
      <nav class="navbar navbar-default navbar-static-top "  role="navigation">
        
        <div class="limiter">
          <!-- hidden nav button --> 
          <div class="navbar-header">
           
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            
            
            
          </div>


          <!-- Main nav buttons -->

            <ul class="hidden-xs" id="mainnav">
<li class="first"><a href="http://local.genesis.com/" title="Home" >Home</a></li>
<li><a href="about-us" title="About Us" >About</a></li>
<li><a href="investment-services" title="Investment Services" >Services</a></li>
<li><a href="developments" title="Developments" >Developments</a></li>
<li class="active"><a href="newsroom" title="Newsroom" >Newsroom</a></li>
<li class="last"><a href="contact" title="Contact" >Contact</a></li>

</ul>


          <!-- Mini nav buttons -->

          <div class="collapse navbar-collapse" id="collapse">
            

            <ul class="nav navbar-nav  visible-xs" id="mininav">
<li class="first"><a href="http://local.genesis.com/" title="Home" >Home</a></li>
<li><a href="about-us" title="About Us" >About</a></li>
<li><a href="investment-services" title="Investment Services" >Services</a></li>
<li><a href="developments" title="Developments" >Developments</a></li>
<li class="active"><a href="newsroom" title="Newsroom" >Newsroom</a></li>
<li class="last"><a href="contact" title="Contact" >Contact</a></li>

</ul>

          </div>
        </div>
      </nav>         
    </div>
  </div>


<div class="row">
    <div class="landing-newsroom" style="background-image:url(\'css/images/newsroom_header.jpg\')">
    <div class="limiter">
          <div class="heading-resources-1  wow fadeInLeft  " data-wow-delay=".2s">
          Newsroom
          </div>
    </div>
  </div>
</div>
<br>
<br>

[[!getImageList?
    &tvname=`articles`
    &tpl=`article`
  ]]


<div class="row">
    <div class="footer">
      &copy;[[!getDate:date=`%Y`]] Genesis Investimentos, LLC. All rights reserved. | <a href="contact.html">Contact Us</a>
    </div>
  </div>



</div>

<!-- javascript !-->

<script src="js/bootstrap.min.js"></script>
<script src="js/wow.js"></script>
<script src="js/main.js"></script>
  <script>
    wow = new WOW(
      {
        animateClass: \'animated\',
        offset:       100
      }
    );
    wow.init();
  
  </script>

</body>
</html>
',
    '_isForward' => false,
  ),
  'contentType' => 
  array (
    'id' => 1,
    'name' => 'HTML',
    'description' => 'HTML content',
    'mime_type' => 'text/html',
    'file_extensions' => '',
    'headers' => NULL,
    'binary' => 0,
  ),
  'policyCache' => 
  array (
  ),
  'elementCache' => 
  array (
    '[[$doc_head]]' => '<!doctype html>
<head>
<meta charset="UTF-8">
<title> Genesis |  </title>
<meta name="description" content="">
<link rel="icon" type="image/jpg" href="favicon.png">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta property="og:title" content=""/>
<meta property="og:image:type" content="image/jpeg" />
<meta property="og:image" content=""/>
<meta property="og:url" content="" />
<meta property="og:description" content=""/>
<link href="css/bootstrap.min.css" rel="stylesheet" />
<link href="css/main.css" rel="stylesheet" type="text/css" />
<script src="js/respond.min.js"></script>
<script src="js/modernizr.custom.js"></script>
<script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
<link href="css/normalize.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="css/animate.css">
<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
<![endif]-->
<script>

(function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,\'script\',\'//www.google-analytics.com/analytics.js\',\'ga\');

ga(\'create\', \'UA-43660403-1\', \'auto\');
ga(\'send\', \'pageview\');

$(function() {
  $(\'a[href*=#]:not([href=#])\').click(function() {
    if (location.pathname.replace(/^\\//,\'\') == this.pathname.replace(/^\\//,\'\') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $(\'[name=\' + this.hash.slice(1) +\']\');
      if (target.length) {
        $(\'html,body\').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});

</script> 
</head>',
    '[[Wayfinder? 
                &level=`1`
                &startId=`0`
                &innerTpl=`nav-innner`
                &outerTpl=`nav-outer-main`
                &sortBy=`menuindex`
                ]]' => '<ul class="hidden-xs" id="mainnav">
<li class="first"><a href="http://local.genesis.com/" title="Home" >Home</a></li>
<li><a href="about-us" title="About Us" >About</a></li>
<li><a href="investment-services" title="Investment Services" >Services</a></li>
<li><a href="developments" title="Developments" >Developments</a></li>
<li class="active"><a href="newsroom" title="Newsroom" >Newsroom</a></li>
<li class="last"><a href="contact" title="Contact" >Contact</a></li>

</ul>
',
    '[[Wayfinder? 
                &level=`1`
                &startId=`0`
                &innerTpl=`nav-innner`
                &outerTpl=`nav-outer-mobile`
                &sortBy=`menuindex`
                ]]' => '<ul class="nav navbar-nav  visible-xs" id="mininav">
<li class="first"><a href="http://local.genesis.com/" title="Home" >Home</a></li>
<li><a href="about-us" title="About Us" >About</a></li>
<li><a href="investment-services" title="Investment Services" >Services</a></li>
<li><a href="developments" title="Developments" >Developments</a></li>
<li class="active"><a href="newsroom" title="Newsroom" >Newsroom</a></li>
<li class="last"><a href="contact" title="Contact" >Contact</a></li>

</ul>',
    '[[$header]]' => '<div class="row">
    <div id="nav">
      <div class="limiter">
        <div class="navbar logobar">
          <div class="headerimage">
            <a href="index.html"><img  src="images/header_logo.png" width="180"  alt=""/></a> 
          </div>
          <div class="header_text pull-right" >Your business partner for Brazilian expansion</div>
        </div>
      </div>
      <nav class="navbar navbar-default navbar-static-top "  role="navigation">
        
        <div class="limiter">
          <!-- hidden nav button --> 
          <div class="navbar-header">
           
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            
            
            
          </div>


          <!-- Main nav buttons -->

            <ul class="hidden-xs" id="mainnav">
<li class="first"><a href="http://local.genesis.com/" title="Home" >Home</a></li>
<li><a href="about-us" title="About Us" >About</a></li>
<li><a href="investment-services" title="Investment Services" >Services</a></li>
<li><a href="developments" title="Developments" >Developments</a></li>
<li class="active"><a href="newsroom" title="Newsroom" >Newsroom</a></li>
<li class="last"><a href="contact" title="Contact" >Contact</a></li>

</ul>


          <!-- Mini nav buttons -->

          <div class="collapse navbar-collapse" id="collapse">
            

            <ul class="nav navbar-nav  visible-xs" id="mininav">
<li class="first"><a href="http://local.genesis.com/" title="Home" >Home</a></li>
<li><a href="about-us" title="About Us" >About</a></li>
<li><a href="investment-services" title="Investment Services" >Services</a></li>
<li><a href="developments" title="Developments" >Developments</a></li>
<li class="active"><a href="newsroom" title="Newsroom" >Newsroom</a></li>
<li class="last"><a href="contact" title="Contact" >Contact</a></li>

</ul>

          </div>
        </div>
      </nav>         
    </div>
  </div>',
    '[[*background-image]]' => 'css/images/newsroom_header.jpg',
    '[[*pagetitle]]' => 'Newsroom',
    '[[$footer]]' => '<div class="row">
    <div class="footer">
      &copy;[[!getDate:date=`%Y`]] Genesis Investimentos, LLC. All rights reserved. | <a href="contact.html">Contact Us</a>
    </div>
  </div>',
  ),
  'sourceCache' => 
  array (
    'modChunk' => 
    array (
      'doc_head' => 
      array (
        'fields' => 
        array (
          'id' => 1,
          'source' => 1,
          'property_preprocess' => false,
          'name' => 'doc_head',
          'description' => '',
          'editor_type' => 0,
          'category' => 3,
          'cache_type' => 0,
          'snippet' => '<!doctype html>
<head>
<meta charset="UTF-8">
<title> Genesis |  </title>
<meta name="description" content="">
<link rel="icon" type="image/jpg" href="favicon.png">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta property="og:title" content=""/>
<meta property="og:image:type" content="image/jpeg" />
<meta property="og:image" content=""/>
<meta property="og:url" content="" />
<meta property="og:description" content=""/>
<link href="css/bootstrap.min.css" rel="stylesheet" />
<link href="css/main.css" rel="stylesheet" type="text/css" />
<script src="js/respond.min.js"></script>
<script src="js/modernizr.custom.js"></script>
<script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
<link href="css/normalize.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="css/animate.css">
<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
<![endif]-->
<script>

(function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,\'script\',\'//www.google-analytics.com/analytics.js\',\'ga\');

ga(\'create\', \'UA-43660403-1\', \'auto\');
ga(\'send\', \'pageview\');

$(function() {
  $(\'a[href*=#]:not([href=#])\').click(function() {
    if (location.pathname.replace(/^\\//,\'\') == this.pathname.replace(/^\\//,\'\') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $(\'[name=\' + this.hash.slice(1) +\']\');
      if (target.length) {
        $(\'html,body\').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});

</script> 
</head>',
          'locked' => false,
          'properties' => 
          array (
          ),
          'static' => true,
          'static_file' => 'assets/chunks/layout/doc_head.html',
          'content' => '<!doctype html>
<head>
<meta charset="UTF-8">
<title> Genesis |  </title>
<meta name="description" content="">
<link rel="icon" type="image/jpg" href="favicon.png">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta property="og:title" content=""/>
<meta property="og:image:type" content="image/jpeg" />
<meta property="og:image" content=""/>
<meta property="og:url" content="" />
<meta property="og:description" content=""/>
<link href="css/bootstrap.min.css" rel="stylesheet" />
<link href="css/main.css" rel="stylesheet" type="text/css" />
<script src="js/respond.min.js"></script>
<script src="js/modernizr.custom.js"></script>
<script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
<link href="css/normalize.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="css/animate.css">
<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
<![endif]-->
<script>

(function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,\'script\',\'//www.google-analytics.com/analytics.js\',\'ga\');

ga(\'create\', \'UA-43660403-1\', \'auto\');
ga(\'send\', \'pageview\');

$(function() {
  $(\'a[href*=#]:not([href=#])\').click(function() {
    if (location.pathname.replace(/^\\//,\'\') == this.pathname.replace(/^\\//,\'\') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $(\'[name=\' + this.hash.slice(1) +\']\');
      if (target.length) {
        $(\'html,body\').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});

</script> 
</head>',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'header' => 
      array (
        'fields' => 
        array (
          'id' => 3,
          'source' => 1,
          'property_preprocess' => false,
          'name' => 'header',
          'description' => '',
          'editor_type' => 0,
          'category' => 3,
          'cache_type' => 0,
          'snippet' => '<div class="row">
    <div id="nav">
      <div class="limiter">
        <div class="navbar logobar">
          <div class="headerimage">
            <a href="index.html"><img  src="images/header_logo.png" width="180"  alt=""/></a> 
          </div>
          <div class="header_text pull-right" >Your business partner for Brazilian expansion</div>
        </div>
      </div>
      <nav class="navbar navbar-default navbar-static-top "  role="navigation">
        
        <div class="limiter">
          <!-- hidden nav button --> 
          <div class="navbar-header">
           
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            
            
            
          </div>


          <!-- Main nav buttons -->

            [[Wayfinder? 
                &level=`1`
                &startId=`0`
                &innerTpl=`nav-innner`
                &outerTpl=`nav-outer-main`
                &sortBy=`menuindex`
                ]]

          <!-- Mini nav buttons -->

          <div class="collapse navbar-collapse" id="collapse">
            

            [[Wayfinder? 
                &level=`1`
                &startId=`0`
                &innerTpl=`nav-innner`
                &outerTpl=`nav-outer-mobile`
                &sortBy=`menuindex`
                ]]

          </div>
        </div>
      </nav>         
    </div>
  </div>',
          'locked' => false,
          'properties' => 
          array (
          ),
          'static' => true,
          'static_file' => 'assets/chunks/layout/header.html',
          'content' => '<div class="row">
    <div id="nav">
      <div class="limiter">
        <div class="navbar logobar">
          <div class="headerimage">
            <a href="index.html"><img  src="images/header_logo.png" width="180"  alt=""/></a> 
          </div>
          <div class="header_text pull-right" >Your business partner for Brazilian expansion</div>
        </div>
      </div>
      <nav class="navbar navbar-default navbar-static-top "  role="navigation">
        
        <div class="limiter">
          <!-- hidden nav button --> 
          <div class="navbar-header">
           
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            
            
            
          </div>


          <!-- Main nav buttons -->

            [[Wayfinder? 
                &level=`1`
                &startId=`0`
                &innerTpl=`nav-innner`
                &outerTpl=`nav-outer-main`
                &sortBy=`menuindex`
                ]]

          <!-- Mini nav buttons -->

          <div class="collapse navbar-collapse" id="collapse">
            

            [[Wayfinder? 
                &level=`1`
                &startId=`0`
                &innerTpl=`nav-innner`
                &outerTpl=`nav-outer-mobile`
                &sortBy=`menuindex`
                ]]

          </div>
        </div>
      </nav>         
    </div>
  </div>',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'footer' => 
      array (
        'fields' => 
        array (
          'id' => 4,
          'source' => 1,
          'property_preprocess' => false,
          'name' => 'footer',
          'description' => '',
          'editor_type' => 0,
          'category' => 3,
          'cache_type' => 0,
          'snippet' => '<div class="row">
    <div class="footer">
      &copy;[[!getDate:date=`%Y`]] Genesis Investimentos, LLC. All rights reserved. | <a href="contact.html">Contact Us</a>
    </div>
  </div>',
          'locked' => false,
          'properties' => 
          array (
          ),
          'static' => true,
          'static_file' => 'assets/chunks/layout/footer.html',
          'content' => '<div class="row">
    <div class="footer">
      &copy;[[!getDate:date=`%Y`]] Genesis Investimentos, LLC. All rights reserved. | <a href="contact.html">Contact Us</a>
    </div>
  </div>',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
    ),
    'modSnippet' => 
    array (
      'Wayfinder' => 
      array (
        'fields' => 
        array (
          'id' => 3,
          'source' => 0,
          'property_preprocess' => false,
          'name' => 'Wayfinder',
          'description' => 'Wayfinder for MODx Revolution 2.0.0-beta-5 and later.',
          'editor_type' => 0,
          'category' => 0,
          'cache_type' => 0,
          'snippet' => '/**
 * Wayfinder Snippet to build site navigation menus
 *
 * Totally refactored from original DropMenu nav builder to make it easier to
 * create custom navigation by using chunks as output templates. By using
 * templates, many of the paramaters are no longer needed for flexible output
 * including tables, unordered- or ordered-lists (ULs or OLs), definition lists
 * (DLs) or in any other format you desire.
 *
 * @version 2.1.1-beta5
 * @author Garry Nutting (collabpad.com)
 * @author Kyle Jaebker (muddydogpaws.com)
 * @author Ryan Thrash (modx.com)
 * @author Shaun McCormick (modx.com)
 * @author Jason Coward (modx.com)
 *
 * @example [[Wayfinder? &startId=`0`]]
 *
 * @var modX $modx
 * @var array $scriptProperties
 * 
 * @package wayfinder
 */
$wayfinder_base = $modx->getOption(\'wayfinder.core_path\',$scriptProperties,$modx->getOption(\'core_path\').\'components/wayfinder/\');

/* include a custom config file if specified */
if (isset($scriptProperties[\'config\'])) {
    $scriptProperties[\'config\'] = str_replace(\'../\',\'\',$scriptProperties[\'config\']);
    $scriptProperties[\'config\'] = $wayfinder_base.\'configs/\'.$scriptProperties[\'config\'].\'.config.php\';
} else {
    $scriptProperties[\'config\'] = $wayfinder_base.\'configs/default.config.php\';
}
if (file_exists($scriptProperties[\'config\'])) {
    include $scriptProperties[\'config\'];
}

/* include wayfinder class */
include_once $wayfinder_base.\'wayfinder.class.php\';
if (!$modx->loadClass(\'Wayfinder\',$wayfinder_base,true,true)) {
    return \'error: Wayfinder class not found\';
}
$wf = new Wayfinder($modx,$scriptProperties);

/* get user class definitions
 * TODO: eventually move these into config parameters */
$wf->_css = array(
    \'first\' => isset($firstClass) ? $firstClass : \'\',
    \'last\' => isset($lastClass) ? $lastClass : \'last\',
    \'here\' => isset($hereClass) ? $hereClass : \'active\',
    \'parent\' => isset($parentClass) ? $parentClass : \'\',
    \'row\' => isset($rowClass) ? $rowClass : \'\',
    \'outer\' => isset($outerClass) ? $outerClass : \'\',
    \'inner\' => isset($innerClass) ? $innerClass : \'\',
    \'level\' => isset($levelClass) ? $levelClass: \'\',
    \'self\' => isset($selfClass) ? $selfClass : \'\',
    \'weblink\' => isset($webLinkClass) ? $webLinkClass : \'\'
);

/* get user templates
 * TODO: eventually move these into config parameters */
$wf->_templates = array(
    \'outerTpl\' => isset($outerTpl) ? $outerTpl : \'\',
    \'rowTpl\' => isset($rowTpl) ? $rowTpl : \'\',
    \'parentRowTpl\' => isset($parentRowTpl) ? $parentRowTpl : \'\',
    \'parentRowHereTpl\' => isset($parentRowHereTpl) ? $parentRowHereTpl : \'\',
    \'hereTpl\' => isset($hereTpl) ? $hereTpl : \'\',
    \'innerTpl\' => isset($innerTpl) ? $innerTpl : \'\',
    \'innerRowTpl\' => isset($innerRowTpl) ? $innerRowTpl : \'\',
    \'innerHereTpl\' => isset($innerHereTpl) ? $innerHereTpl : \'\',
    \'activeParentRowTpl\' => isset($activeParentRowTpl) ? $activeParentRowTpl : \'\',
    \'categoryFoldersTpl\' => isset($categoryFoldersTpl) ? $categoryFoldersTpl : \'\',
    \'startItemTpl\' => isset($startItemTpl) ? $startItemTpl : \'\'
);

/* process Wayfinder */
$output = $wf->run();
if ($wf->_config[\'debug\']) {
    $output .= $wf->renderDebugOutput();
}

/* output results */
if ($wf->_config[\'ph\']) {
    $modx->setPlaceholder($wf->_config[\'ph\'],$output);
} else {
    return $output;
}',
          'locked' => false,
          'properties' => 
          array (
            'level' => 
            array (
              'name' => 'level',
              'desc' => 'prop_wayfinder.level_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '0',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Depth (number of levels) to build the menu from. 0 goes through all levels.',
              'area' => '',
              'area_trans' => '',
            ),
            'includeDocs' => 
            array (
              'name' => 'includeDocs',
              'desc' => 'prop_wayfinder.includeDocs_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '0',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Acts as a filter and will limit the output to only the documents specified in this parameter. The startId is still required.',
              'area' => '',
              'area_trans' => '',
            ),
            'excludeDocs' => 
            array (
              'name' => 'excludeDocs',
              'desc' => 'prop_wayfinder.excludeDocs_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '0',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Acts as a filter and will remove the documents specified in this parameter from the output. The startId is still required.',
              'area' => '',
              'area_trans' => '',
            ),
            'contexts' => 
            array (
              'name' => 'contexts',
              'desc' => 'prop_wayfinder.contexts_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Specify the contexts for the Resources that will be loaded in this menu. Useful when used with startId at 0 to show all first-level items. Note: This will increase load times a bit, but if you set cacheResults to 1, that will offset the load time.',
              'area' => '',
              'area_trans' => '',
            ),
            'cacheResults' => 
            array (
              'name' => 'cacheResults',
              'desc' => 'prop_wayfinder.cacheResults_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => true,
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Cache the generated menu to the MODX Resource cache. Setting this to 1 will speed up the loading of your menus.',
              'area' => '',
              'area_trans' => '',
            ),
            'cacheTime' => 
            array (
              'name' => 'cacheTime',
              'desc' => 'prop_wayfinder.cacheTime_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 3600,
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'The number of seconds to store the cached menu, if cacheResults is 1. Set to 0 to store indefinitely until cache is manually cleared.',
              'area' => '',
              'area_trans' => '',
            ),
            'ph' => 
            array (
              'name' => 'ph',
              'desc' => 'prop_wayfinder.ph_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'To display send the output of Wayfinder to a placeholder set the ph parameter equal to the name of the desired placeholder. All output including the debugging (if on) will be sent to the placeholder specified.',
              'area' => '',
              'area_trans' => '',
            ),
            'debug' => 
            array (
              'name' => 'debug',
              'desc' => 'prop_wayfinder.debug_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'With the debug parameter set to 1, Wayfinder will output information on how each Resource was processed.',
              'area' => '',
              'area_trans' => '',
            ),
            'ignoreHidden' => 
            array (
              'name' => 'ignoreHidden',
              'desc' => 'prop_wayfinder.ignoreHidden_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'The ignoreHidden parameter allows Wayfinder to ignore the display in menu flag that can be set for each document. With this parameter set to 1, all Resources will be displayed regardless of the Display in Menu flag.',
              'area' => '',
              'area_trans' => '',
            ),
            'hideSubMenus' => 
            array (
              'name' => 'hideSubMenus',
              'desc' => 'prop_wayfinder.hideSubMenus_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'The hideSubMenus parameter will remove all non-active submenus from the Wayfinder output if set to 1. This parameter only works if multiple levels are being displayed.',
              'area' => '',
              'area_trans' => '',
            ),
            'useWeblinkUrl' => 
            array (
              'name' => 'useWeblinkUrl',
              'desc' => 'prop_wayfinder.useWeblinkUrl_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => true,
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => ' If WebLinks are used in the output, Wayfinder will output the link specified in the WebLink instead of the normal MODx link. To use the standard display of WebLinks (like any other Resource) set this to 0.',
              'area' => '',
              'area_trans' => '',
            ),
            'fullLink' => 
            array (
              'name' => 'fullLink',
              'desc' => 'prop_wayfinder.fullLink_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'If set to 1, will display the entire, absolute URL in the link. (It is recommended to use scheme instead.)',
              'area' => '',
              'area_trans' => '',
            ),
            'scheme' => 
            array (
              'name' => 'scheme',
              'desc' => 'prop_wayfinder.scheme_desc',
              'type' => 'list',
              'options' => 
              array (
                0 => 
                array (
                  'text' => 'prop_wayfinder.relative',
                  'value' => '',
                  'name' => 'Relative',
                ),
                1 => 
                array (
                  'text' => 'prop_wayfinder.absolute',
                  'value' => 'abs',
                  'name' => 'Absolute',
                ),
                2 => 
                array (
                  'text' => 'prop_wayfinder.full',
                  'value' => 'full',
                  'name' => 'Full',
                ),
              ),
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Determines how URLs are generated for each link. Set to "abs" to show the absolute URL, "full" to show the full URL, and blank to use the relative URL. Defaults to relative.',
              'area' => '',
              'area_trans' => '',
            ),
            'sortOrder' => 
            array (
              'name' => 'sortOrder',
              'desc' => 'prop_wayfinder.sortOrder_desc',
              'type' => 'list',
              'options' => 
              array (
                0 => 
                array (
                  'text' => 'prop_wayfinder.ascending',
                  'value' => 'ASC',
                  'name' => 'Ascending',
                ),
                1 => 
                array (
                  'text' => 'prop_wayfinder.descending',
                  'value' => 'DESC',
                  'name' => 'Descending',
                ),
              ),
              'value' => 'ASC',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Allows the menu to be sorted in either ascending or descending order.',
              'area' => '',
              'area_trans' => '',
            ),
            'sortBy' => 
            array (
              'name' => 'sortBy',
              'desc' => 'prop_wayfinder.sortBy_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'menuindex',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Sorts the output by any of the Resource fields on a level by level basis. This means that each submenu will be sorted independently of all other submenus at the same level. Random will sort the output differently every time the page is loaded if the snippet is called uncached.',
              'area' => '',
              'area_trans' => '',
            ),
            'limit' => 
            array (
              'name' => 'limit',
              'desc' => 'prop_wayfinder.limit_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '0',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Causes Wayfinder to only process the number of items specified per level.',
              'area' => '',
              'area_trans' => '',
            ),
            'cssTpl' => 
            array (
              'name' => 'cssTpl',
              'desc' => 'prop_wayfinder.cssTpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => false,
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'This parameter allows for a chunk containing a link to a style sheet or style information to be inserted into the head section of the generated page.',
              'area' => '',
              'area_trans' => '',
            ),
            'jsTpl' => 
            array (
              'name' => 'jsTpl',
              'desc' => 'prop_wayfinder.jsTpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => false,
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'This parameter allows for a chunk containing some Javascript to be inserted into the head section of the generated page.',
              'area' => '',
              'area_trans' => '',
            ),
            'rowIdPrefix' => 
            array (
              'name' => 'rowIdPrefix',
              'desc' => 'prop_wayfinder.rowIdPrefix_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => false,
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'If set, Wayfinder will replace the id placeholder with a unique id consisting of the specified prefix plus the Resource id.',
              'area' => '',
              'area_trans' => '',
            ),
            'textOfLinks' => 
            array (
              'name' => 'textOfLinks',
              'desc' => 'prop_wayfinder.textOfLinks_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'menutitle',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'This field will be inserted into the linktext placeholder.',
              'area' => '',
              'area_trans' => '',
            ),
            'titleOfLinks' => 
            array (
              'name' => 'titleOfLinks',
              'desc' => 'prop_wayfinder.titleOfLinks_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'pagetitle',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'This field will be inserted into the linktitle placeholder.',
              'area' => '',
              'area_trans' => '',
            ),
            'displayStart' => 
            array (
              'name' => 'displayStart',
              'desc' => 'prop_wayfinder.displayStart_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Show the document as referenced by startId in the menu.',
              'area' => '',
              'area_trans' => '',
            ),
            'firstClass' => 
            array (
              'name' => 'firstClass',
              'desc' => 'prop_wayfinder.firstClass_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'first',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'CSS class for the first item at a given menu level.',
              'area' => '',
              'area_trans' => '',
            ),
            'lastClass' => 
            array (
              'name' => 'lastClass',
              'desc' => 'prop_wayfinder.lastClass_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'last',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'CSS class for the last item at a given menu level.',
              'area' => '',
              'area_trans' => '',
            ),
            'hereClass' => 
            array (
              'name' => 'hereClass',
              'desc' => 'prop_wayfinder.hereClass_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'active',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'CSS class for the items showing where you are, all the way up the chain.',
              'area' => '',
              'area_trans' => '',
            ),
            'parentClass' => 
            array (
              'name' => 'parentClass',
              'desc' => 'prop_wayfinder.parentClass_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'CSS class for menu items that are a container and have children.',
              'area' => '',
              'area_trans' => '',
            ),
            'rowClass' => 
            array (
              'name' => 'rowClass',
              'desc' => 'prop_wayfinder.rowClass_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'CSS class denoting each output row.',
              'area' => '',
              'area_trans' => '',
            ),
            'outerClass' => 
            array (
              'name' => 'outerClass',
              'desc' => 'prop_wayfinder.outerClass_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'CSS class for the outer template.',
              'area' => '',
              'area_trans' => '',
            ),
            'innerClass' => 
            array (
              'name' => 'innerClass',
              'desc' => 'prop_wayfinder.innerClass_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'CSS class for the inner template.',
              'area' => '',
              'area_trans' => '',
            ),
            'levelClass' => 
            array (
              'name' => 'levelClass',
              'desc' => 'prop_wayfinder.levelClass_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'CSS class denoting every output row level. The level number will be added to the specified class (level1, level2, level3 etc if you specified "level").',
              'area' => '',
              'area_trans' => '',
            ),
            'selfClass' => 
            array (
              'name' => 'selfClass',
              'desc' => 'prop_wayfinder.selfClass_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'CSS class for the current item.',
              'area' => '',
              'area_trans' => '',
            ),
            'webLinkClass' => 
            array (
              'name' => 'webLinkClass',
              'desc' => 'prop_wayfinder.webLinkClass_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'CSS class for weblink items.',
              'area' => '',
              'area_trans' => '',
            ),
            'outerTpl' => 
            array (
              'name' => 'outerTpl',
              'desc' => 'prop_wayfinder.outerTpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Name of the chunk containing the template for the outer most container; if not included, a string including "<ul>[[+wf.wrapper]]</ul>" is assumed.',
              'area' => '',
              'area_trans' => '',
            ),
            'rowTpl' => 
            array (
              'name' => 'rowTpl',
              'desc' => 'prop_wayfinder.rowTpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Name of the chunk containing the template for the regular row items.',
              'area' => '',
              'area_trans' => '',
            ),
            'parentRowTpl' => 
            array (
              'name' => 'parentRowTpl',
              'desc' => 'prop_wayfinder.parentRowTpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Name of the chunk containing the template for any Resource that is a container and has children. Remember the [wf.wrapper] placeholder to output the children documents.',
              'area' => '',
              'area_trans' => '',
            ),
            'parentRowHereTpl' => 
            array (
              'name' => 'parentRowHereTpl',
              'desc' => 'prop_wayfinder.parentRowHereTpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Name of the chunk containing the template for the current Resource if it is a container and has children. Remember the [wf.wrapper] placeholder to output the children documents.',
              'area' => '',
              'area_trans' => '',
            ),
            'hereTpl' => 
            array (
              'name' => 'hereTpl',
              'desc' => 'prop_wayfinder.hereTpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Name of the chunk containing the template for the current Resource.',
              'area' => '',
              'area_trans' => '',
            ),
            'innerTpl' => 
            array (
              'name' => 'innerTpl',
              'desc' => 'prop_wayfinder.innerTpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Name of the chunk containing the template for each submenu. If no innerTpl is specified the outerTpl is used in its place.',
              'area' => '',
              'area_trans' => '',
            ),
            'innerRowTpl' => 
            array (
              'name' => 'innerRowTpl',
              'desc' => 'prop_wayfinder.innerRowTpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Name of the chunk containing the template for the row items in a subfolder.',
              'area' => '',
              'area_trans' => '',
            ),
            'innerHereTpl' => 
            array (
              'name' => 'innerHereTpl',
              'desc' => 'prop_wayfinder.innerHereTpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Name of the chunk containing the template for the current Resource if it is in a subfolder.',
              'area' => '',
              'area_trans' => '',
            ),
            'activeParentRowTpl' => 
            array (
              'name' => 'activeParentRowTpl',
              'desc' => 'prop_wayfinder.activeParentRowTpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Name of the chunk containing the template for items that are containers, have children and are currently active in the tree.',
              'area' => '',
              'area_trans' => '',
            ),
            'categoryFoldersTpl' => 
            array (
              'name' => 'categoryFoldersTpl',
              'desc' => 'prop_wayfinder.categoryFoldersTpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Name of the chunk containing the template for category folders. Category folders are determined by setting the template to blank or by setting the link attributes field to rel="category".',
              'area' => '',
              'area_trans' => '',
            ),
            'startItemTpl' => 
            array (
              'name' => 'startItemTpl',
              'desc' => 'prop_wayfinder.startItemTpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Name of the chunk containing the template for the start item, if enabled via the &displayStart parameter. Note: the default template shows the start item but does not link it. If you do not need a link, a class can be applied to the default template using the parameter &firstClass=`className`.',
              'area' => '',
              'area_trans' => '',
            ),
            'permissions' => 
            array (
              'name' => 'permissions',
              'desc' => 'prop_wayfinder.permissions_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'list',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Will check for a permission on the Resource. Defaults to "list" - set to blank to skip normal permissions checks.',
              'area' => '',
              'area_trans' => '',
            ),
            'hereId' => 
            array (
              'name' => 'hereId',
              'desc' => 'prop_wayfinder.hereId_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Optional. If set, will change the "here" Resource to this ID. Defaults to the currently active Resource.',
              'area' => '',
              'area_trans' => '',
            ),
            'where' => 
            array (
              'name' => 'where',
              'desc' => 'prop_wayfinder.where_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Optional. A JSON object for where conditions for all items selected in the menu.',
              'area' => '',
              'area_trans' => '',
            ),
            'templates' => 
            array (
              'name' => 'templates',
              'desc' => 'prop_wayfinder.templates_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Optional. A comma-separated list of Template IDs to restrict selected Resources to.',
              'area' => '',
              'area_trans' => '',
            ),
            'previewUnpublished' => 
            array (
              'name' => 'previewUnpublished',
              'desc' => 'prop_wayfinder.previewunpublished_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'wayfinder:properties',
              'desc_trans' => 'Optional. If set to Yes, if you are logged into the mgr and have the view_unpublished permission, it will allow previewing of unpublished resources in your menus in the front-end.',
              'area' => '',
              'area_trans' => '',
            ),
          ),
          'moduleguid' => '',
          'static' => false,
          'static_file' => '',
          'content' => '/**
 * Wayfinder Snippet to build site navigation menus
 *
 * Totally refactored from original DropMenu nav builder to make it easier to
 * create custom navigation by using chunks as output templates. By using
 * templates, many of the paramaters are no longer needed for flexible output
 * including tables, unordered- or ordered-lists (ULs or OLs), definition lists
 * (DLs) or in any other format you desire.
 *
 * @version 2.1.1-beta5
 * @author Garry Nutting (collabpad.com)
 * @author Kyle Jaebker (muddydogpaws.com)
 * @author Ryan Thrash (modx.com)
 * @author Shaun McCormick (modx.com)
 * @author Jason Coward (modx.com)
 *
 * @example [[Wayfinder? &startId=`0`]]
 *
 * @var modX $modx
 * @var array $scriptProperties
 * 
 * @package wayfinder
 */
$wayfinder_base = $modx->getOption(\'wayfinder.core_path\',$scriptProperties,$modx->getOption(\'core_path\').\'components/wayfinder/\');

/* include a custom config file if specified */
if (isset($scriptProperties[\'config\'])) {
    $scriptProperties[\'config\'] = str_replace(\'../\',\'\',$scriptProperties[\'config\']);
    $scriptProperties[\'config\'] = $wayfinder_base.\'configs/\'.$scriptProperties[\'config\'].\'.config.php\';
} else {
    $scriptProperties[\'config\'] = $wayfinder_base.\'configs/default.config.php\';
}
if (file_exists($scriptProperties[\'config\'])) {
    include $scriptProperties[\'config\'];
}

/* include wayfinder class */
include_once $wayfinder_base.\'wayfinder.class.php\';
if (!$modx->loadClass(\'Wayfinder\',$wayfinder_base,true,true)) {
    return \'error: Wayfinder class not found\';
}
$wf = new Wayfinder($modx,$scriptProperties);

/* get user class definitions
 * TODO: eventually move these into config parameters */
$wf->_css = array(
    \'first\' => isset($firstClass) ? $firstClass : \'\',
    \'last\' => isset($lastClass) ? $lastClass : \'last\',
    \'here\' => isset($hereClass) ? $hereClass : \'active\',
    \'parent\' => isset($parentClass) ? $parentClass : \'\',
    \'row\' => isset($rowClass) ? $rowClass : \'\',
    \'outer\' => isset($outerClass) ? $outerClass : \'\',
    \'inner\' => isset($innerClass) ? $innerClass : \'\',
    \'level\' => isset($levelClass) ? $levelClass: \'\',
    \'self\' => isset($selfClass) ? $selfClass : \'\',
    \'weblink\' => isset($webLinkClass) ? $webLinkClass : \'\'
);

/* get user templates
 * TODO: eventually move these into config parameters */
$wf->_templates = array(
    \'outerTpl\' => isset($outerTpl) ? $outerTpl : \'\',
    \'rowTpl\' => isset($rowTpl) ? $rowTpl : \'\',
    \'parentRowTpl\' => isset($parentRowTpl) ? $parentRowTpl : \'\',
    \'parentRowHereTpl\' => isset($parentRowHereTpl) ? $parentRowHereTpl : \'\',
    \'hereTpl\' => isset($hereTpl) ? $hereTpl : \'\',
    \'innerTpl\' => isset($innerTpl) ? $innerTpl : \'\',
    \'innerRowTpl\' => isset($innerRowTpl) ? $innerRowTpl : \'\',
    \'innerHereTpl\' => isset($innerHereTpl) ? $innerHereTpl : \'\',
    \'activeParentRowTpl\' => isset($activeParentRowTpl) ? $activeParentRowTpl : \'\',
    \'categoryFoldersTpl\' => isset($categoryFoldersTpl) ? $categoryFoldersTpl : \'\',
    \'startItemTpl\' => isset($startItemTpl) ? $startItemTpl : \'\'
);

/* process Wayfinder */
$output = $wf->run();
if ($wf->_config[\'debug\']) {
    $output .= $wf->renderDebugOutput();
}

/* output results */
if ($wf->_config[\'ph\']) {
    $modx->setPlaceholder($wf->_config[\'ph\'],$output);
} else {
    return $output;
}',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
        ),
      ),
      'getImageList' => 
      array (
        'fields' => 
        array (
          'id' => 17,
          'source' => 0,
          'property_preprocess' => false,
          'name' => 'getImageList',
          'description' => '',
          'editor_type' => 0,
          'category' => 9,
          'cache_type' => 0,
          'snippet' => '/**
 * getImageList
 *
 * Copyright 2009-2014 by Bruno Perner <b.perner@gmx.de>
 *
 * getImageList is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 *
 * getImageList is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * getImageList; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA 02111-1307 USA
 *
 * @package migx
 */
/**
 * getImageList
 *
 * display Items from outputvalue of TV with custom-TV-input-type MIGX or from other JSON-string for MODx Revolution 
 *
 * @version 1.4
 * @author Bruno Perner <b.perner@gmx.de>
 * @copyright Copyright &copy; 2009-2014
 * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0.html GNU General Public License
 * version 2 or (at your option) any later version.
 * @package migx
 */

/*example: <ul>[[!getImageList? &tvname=`myTV`&tpl=`@CODE:<li>[[+idx]]<img src="[[+imageURL]]"/><p>[[+imageAlt]]</p></li>`]]</ul>*/
/* get default properties */


$tvname = $modx->getOption(\'tvname\', $scriptProperties, \'\');
$tpl = $modx->getOption(\'tpl\', $scriptProperties, \'\');
$wrapperTpl = $modx->getOption(\'wrapperTpl\', $scriptProperties, \'\');
$limit = $modx->getOption(\'limit\', $scriptProperties, \'0\');
$offset = $modx->getOption(\'offset\', $scriptProperties, 0);
$totalVar = $modx->getOption(\'totalVar\', $scriptProperties, \'total\');
$randomize = $modx->getOption(\'randomize\', $scriptProperties, false);
$preselectLimit = $modx->getOption(\'preselectLimit\', $scriptProperties, 0); // when random preselect important images
$where = $modx->getOption(\'where\', $scriptProperties, \'\');
$where = !empty($where) ? $modx->fromJSON($where) : array();
$sort = $modx->getOption(\'sort\', $scriptProperties, \'\');
$sort = !empty($sort) ? $modx->fromJSON($sort) : array();
$toSeparatePlaceholders = $modx->getOption(\'toSeparatePlaceholders\', $scriptProperties, false);
$toPlaceholder = $modx->getOption(\'toPlaceholder\', $scriptProperties, false);
$outputSeparator = $modx->getOption(\'outputSeparator\', $scriptProperties, \'\');
$splitSeparator = $modx->getOption(\'splitSeparator\', $scriptProperties, \'\');
$placeholdersKeyField = $modx->getOption(\'placeholdersKeyField\', $scriptProperties, \'MIGX_id\');
$toJsonPlaceholder = $modx->getOption(\'toJsonPlaceholder\', $scriptProperties, false);
$jsonVarKey = $modx->getOption(\'jsonVarKey\', $scriptProperties, \'migx_outputvalue\');
$outputvalue = $modx->getOption(\'value\', $scriptProperties, \'\');
$outputvalue = isset($_REQUEST[$jsonVarKey]) ? $_REQUEST[$jsonVarKey] : $outputvalue;
$docidVarKey = $modx->getOption(\'docidVarKey\', $scriptProperties, \'migx_docid\');
$docid = $modx->getOption(\'docid\', $scriptProperties, (isset($modx->resource) ? $modx->resource->get(\'id\') : 1));
$docid = isset($_REQUEST[$docidVarKey]) ? $_REQUEST[$docidVarKey] : $docid;
$processTVs = $modx->getOption(\'processTVs\', $scriptProperties, \'1\');
$reverse = $modx->getOption(\'reverse\', $scriptProperties, \'0\');
$sumFields = $modx->getOption(\'sumFields\', $scriptProperties, \'\');
$sumPrefix = $modx->getOption(\'sumPrefix\', $scriptProperties, \'summary_\');
$addfields = $modx->getOption(\'addfields\', $scriptProperties, \'\');
$addfields = !empty($addfields) ? explode(\',\', $addfields) : null;
//split json into parts
$splits = $modx->fromJson($modx->getOption(\'splits\', $scriptProperties, 0));
$splitTpl = $modx->getOption(\'splitTpl\', $scriptProperties, \'\');
$splitSeparator = $modx->getOption(\'splitSeparator\', $scriptProperties, \'\');
$inheritFrom = $modx->getOption(\'inheritFrom\', $scriptProperties, \'\'); //commaseparated list of resource-ids or/and the keyword \'parents\' where to inherit from
$inheritFrom = !empty($inheritFrom) ? explode(\',\',$inheritFrom) : \'\';

$modx->setPlaceholder(\'docid\', $docid);

$base_path = $modx->getOption(\'base_path\', null, MODX_BASE_PATH);
$base_url = $modx->getOption(\'base_url\', null, MODX_BASE_URL);

$migx = $modx->getService(\'migx\', \'Migx\', $modx->getOption(\'migx.core_path\', null, $modx->getOption(\'core_path\') . \'components/migx/\') . \'model/migx/\', $scriptProperties);
if (!($migx instanceof Migx))
    return \'\';
$migx->working_context = isset($modx->resource) ? $modx->resource->get(\'context_key\') : \'web\';

if (!empty($tvname)) {
    if ($tv = $modx->getObject(\'modTemplateVar\', array(\'name\' => $tvname))) {

        /*
        *   get inputProperties
        */


        $properties = $tv->get(\'input_properties\');
        $properties = isset($properties[\'formtabs\']) ? $properties : $tv->getProperties();

        $migx->config[\'configs\'] = $modx->getOption(\'configs\', $properties, \'\');
        if (!empty($migx->config[\'configs\'])) {
            $migx->loadConfigs();
            // get tabs from file or migx-config-table
            $formtabs = $migx->getTabs();
        }
        if (empty($formtabs) && isset($properties[\'formtabs\'])) {
            //try to get formtabs and its fields from properties
            $formtabs = $modx->fromJSON($properties[\'formtabs\']);
        }

        if (!empty($properties[\'basePath\'])) {
            if ($properties[\'autoResourceFolders\'] == \'true\') {
                $scriptProperties[\'base_path\'] = $base_path . $properties[\'basePath\'] . $docid . \'/\';
                $scriptProperties[\'base_url\'] = $base_url . $properties[\'basePath\'] . $docid . \'/\';
            } else {
                $scriptProperties[\'base_path\'] = $base_path . $properties[\'base_path\'];
                $scriptProperties[\'base_url\'] = $base_url . $properties[\'basePath\'];
            }
        }
        if ($jsonVarKey == \'migx_outputvalue\' && !empty($properties[\'jsonvarkey\'])) {
            $jsonVarKey = $properties[\'jsonvarkey\'];
            $outputvalue = isset($_REQUEST[$jsonVarKey]) ? $_REQUEST[$jsonVarKey] : $outputvalue;
        }
        
        if (empty($outputvalue)){
            $outputvalue = $tv->renderOutput($docid);
            if (empty($outputvalue) && !empty($inheritFrom)){
                foreach ($inheritFrom as $from){
                    if ($from == \'parents\'){
                        $outputvalue = $tv->processInheritBinding(\'\',$docid);
                    }else{
                        $outputvalue = $tv->renderOutput($from);
                    }
                    if (!empty($outputvalue)){
                        break;
                    }                    
                }
            }
        }
        
        
        /*
        *   get inputTvs 
        */
        $inputTvs = array();
        if (is_array($formtabs)) {

            //multiple different Forms
            // Note: use same field-names and inputTVs in all forms
            $inputTvs = $migx->extractInputTvs($formtabs);
        }
        if ($migx->source = $tv->getSource($migx->working_context, false)){
            $migx->source->initialize();
        }
        
    }


}

if (empty($outputvalue)) {
    return \'\';
}

//echo $outputvalue.\'<br/><br/>\';

$items = $modx->fromJSON($outputvalue);

// where filter
if (is_array($where) && count($where) > 0) {
    $items = $migx->filterItems($where, $items);
}
$modx->setPlaceholder($totalVar, count($items));

if (!empty($reverse)) {
    $items = array_reverse($items);
}

// sort items
if (is_array($sort) && count($sort) > 0) {
    $items = $migx->sortDbResult($items, $sort);
}

$summaries = array();
$output = \'\';
$items = $offset > 0 ? array_slice($items, $offset) : $items;
$count = count($items);

if ($count > 0) {
    $limit = $limit == 0 || $limit > $count ? $count : $limit;
    $preselectLimit = $preselectLimit > $count ? $count : $preselectLimit;
    //preselect important items
    $preitems = array();
    if ($randomize && $preselectLimit > 0) {
        for ($i = 0; $i < $preselectLimit; $i++) {
            $preitems[] = $items[$i];
            unset($items[$i]);
        }
        $limit = $limit - count($preitems);
    }

    //shuffle items
    if ($randomize) {
        shuffle($items);
    }

    //limit items
    $count = count($items);
    $tempitems = array();

    for ($i = 0; $i < $limit; $i++) {
        if ($i >= $count) {
            break;
        }
        $tempitems[] = $items[$i];
    }
    $items = $tempitems;

    //add preselected items and schuffle again
    if ($randomize && $preselectLimit > 0) {
        $items = array_merge($preitems, $items);
        shuffle($items);
    }

    $properties = array();
    foreach ($scriptProperties as $property => $value) {
        $properties[\'property.\' . $property] = $value;
    }

    $idx = 0;
    $output = array();
    $template = array();
    $count = count($items);

    foreach ($items as $key => $item) {
        $formname = isset($item[\'MIGX_formname\']) ? $item[\'MIGX_formname\'] . \'_\' : \'\';
        $fields = array();
        foreach ($item as $field => $value) {
            if (is_array($value)) {
                if (is_array($value[0])) {
                    //nested array - convert to json
                    $value = $modx->toJson($value);
                } else {
                    $value = implode(\'||\', $value); //handle arrays (checkboxes, multiselects)
                }
            }


            $inputTVkey = $formname . $field;
            if ($processTVs && isset($inputTvs[$inputTVkey])) {
                if (isset($inputTvs[$inputTVkey][\'inputTV\']) && $tv = $modx->getObject(\'modTemplateVar\', array(\'name\' => $inputTvs[$inputTVkey][\'inputTV\']))) {

                } else {
                    $tv = $modx->newObject(\'modTemplateVar\');
                    $tv->set(\'type\', $inputTvs[$inputTVkey][\'inputTVtype\']);
                }
                $inputTV = $inputTvs[$inputTVkey];

                $mTypes = $modx->getOption(\'manipulatable_url_tv_output_types\', null, \'image,file\');
                //don\'t manipulate any urls here
                $modx->setOption(\'manipulatable_url_tv_output_types\', \'\');
                $tv->set(\'default_text\', $value);
                $value = $tv->renderOutput($docid);
                //set option back
                $modx->setOption(\'manipulatable_url_tv_output_types\', $mTypes);
                //now manipulate urls
                if ($mediasource = $migx->getFieldSource($inputTV, $tv)) {
                    $mTypes = explode(\',\', $mTypes);
                    if (!empty($value) && in_array($tv->get(\'type\'), $mTypes)) {
                        //$value = $mediasource->prepareOutputUrl($value);
                        $value = str_replace(\'/./\', \'/\', $mediasource->prepareOutputUrl($value));
                    }
                }

            }
            $fields[$field] = $value;

        }

        if (!empty($addfields)) {
            foreach ($addfields as $addfield) {
                $addfield = explode(\':\', $addfield);
                $addname = $addfield[0];
                $adddefault = isset($addfield[1]) ? $addfield[1] : \'\';
                $fields[$addname] = $adddefault;
            }
        }

        if (!empty($sumFields)) {
            $sumFields = is_array($sumFields) ? $sumFields : explode(\',\', $sumFields);
            foreach ($sumFields as $sumField) {
                if (isset($fields[$sumField])) {
                    $summaries[$sumPrefix . $sumField] = $summaries[$sumPrefix . $sumField] + $fields[$sumField];
                    $fields[$sumPrefix . $sumField] = $summaries[$sumPrefix . $sumField];
                }
            }
        }


        if ($toJsonPlaceholder) {
            $output[] = $fields;
        } else {
            $fields[\'_alt\'] = $idx % 2;
            $idx++;
            $fields[\'_first\'] = $idx == 1 ? true : \'\';
            $fields[\'_last\'] = $idx == $limit ? true : \'\';
            $fields[\'idx\'] = $idx;
            $rowtpl = \'\';
            //get changing tpls from field
            if (substr($tpl, 0, 7) == "@FIELD:") {
                $tplField = substr($tpl, 7);
                $rowtpl = $fields[$tplField];
            }

            if ($fields[\'_first\'] && !empty($tplFirst)) {
                $rowtpl = $tplFirst;
            }
            if ($fields[\'_last\'] && empty($rowtpl) && !empty($tplLast)) {
                $rowtpl = $tplLast;
            }
            $tplidx = \'tpl_\' . $idx;
            if (empty($rowtpl) && !empty($$tplidx)) {
                $rowtpl = $$tplidx;
            }
            if ($idx > 1 && empty($rowtpl)) {
                $divisors = $migx->getDivisors($idx);
                if (!empty($divisors)) {
                    foreach ($divisors as $divisor) {
                        $tplnth = \'tpl_n\' . $divisor;
                        if (!empty($$tplnth)) {
                            $rowtpl = $$tplnth;
                            if (!empty($rowtpl)) {
                                break;
                            }
                        }
                    }
                }
            }

            if ($count == 1 && isset($tpl_oneresult)) {
                $rowtpl = $tpl_oneresult;
            }

            $fields = array_merge($fields, $properties);

            if (!empty($rowtpl)) {
                $template = $migx->getTemplate($tpl, $template);
                $fields[\'_tpl\'] = $template[$tpl];
            } else {
                $rowtpl = $tpl;

            }
            $template = $migx->getTemplate($rowtpl, $template);


            if ($template[$rowtpl]) {
                $chunk = $modx->newObject(\'modChunk\');
                $chunk->setCacheable(false);
                $chunk->setContent($template[$rowtpl]);


                if (!empty($placeholdersKeyField) && isset($fields[$placeholdersKeyField])) {
                    $output[$fields[$placeholdersKeyField]] = $chunk->process($fields);
                } else {
                    $output[] = $chunk->process($fields);
                }
            } else {
                if (!empty($placeholdersKeyField)) {
                    $output[$fields[$placeholdersKeyField]] = \'<pre>\' . print_r($fields, 1) . \'</pre>\';
                } else {
                    $output[] = \'<pre>\' . print_r($fields, 1) . \'</pre>\';
                }
            }
        }


    }
}

if (count($summaries) > 0) {
    $modx->toPlaceholders($summaries);
}


if ($toJsonPlaceholder) {
    $modx->setPlaceholder($toJsonPlaceholder, $modx->toJson($output));
    return \'\';
}

if (!empty($toSeparatePlaceholders)) {
    $modx->toPlaceholders($output, $toSeparatePlaceholders);
    return \'\';
}
/*
if (!empty($outerTpl))
$o = parseTpl($outerTpl, array(\'output\'=>implode($outputSeparator, $output)));
else 
*/

if ($count > 0 && $splits > 0) {
    $size = ceil($count / $splits);
    $chunks = array_chunk($output, $size);
    $output = array();
    foreach ($chunks as $chunk) {
        $o = implode($outputSeparator, $chunk);
        $output[] = $modx->getChunk($splitTpl, array(\'output\' => $o));
    }
    $outputSeparator = $splitSeparator;
}

if (is_array($output)) {
    $o = implode($outputSeparator, $output);
} else {
    $o = $output;
}

if (!empty($o) && !empty($wrapperTpl)) {
    $template = $migx->getTemplate($wrapperTpl);
    if ($template[$wrapperTpl]) {
        $chunk = $modx->newObject(\'modChunk\');
        $chunk->setCacheable(false);
        $chunk->setContent($template[$wrapperTpl]);
        $properties[\'output\'] = $o;
        $o = $chunk->process($properties);
    }
}

if (!empty($toPlaceholder)) {
    $modx->setPlaceholder($toPlaceholder, $o);
    return \'\';
}

return $o;',
          'locked' => false,
          'properties' => 
          array (
          ),
          'moduleguid' => '',
          'static' => false,
          'static_file' => '',
          'content' => '/**
 * getImageList
 *
 * Copyright 2009-2014 by Bruno Perner <b.perner@gmx.de>
 *
 * getImageList is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 *
 * getImageList is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * getImageList; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA 02111-1307 USA
 *
 * @package migx
 */
/**
 * getImageList
 *
 * display Items from outputvalue of TV with custom-TV-input-type MIGX or from other JSON-string for MODx Revolution 
 *
 * @version 1.4
 * @author Bruno Perner <b.perner@gmx.de>
 * @copyright Copyright &copy; 2009-2014
 * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0.html GNU General Public License
 * version 2 or (at your option) any later version.
 * @package migx
 */

/*example: <ul>[[!getImageList? &tvname=`myTV`&tpl=`@CODE:<li>[[+idx]]<img src="[[+imageURL]]"/><p>[[+imageAlt]]</p></li>`]]</ul>*/
/* get default properties */


$tvname = $modx->getOption(\'tvname\', $scriptProperties, \'\');
$tpl = $modx->getOption(\'tpl\', $scriptProperties, \'\');
$wrapperTpl = $modx->getOption(\'wrapperTpl\', $scriptProperties, \'\');
$limit = $modx->getOption(\'limit\', $scriptProperties, \'0\');
$offset = $modx->getOption(\'offset\', $scriptProperties, 0);
$totalVar = $modx->getOption(\'totalVar\', $scriptProperties, \'total\');
$randomize = $modx->getOption(\'randomize\', $scriptProperties, false);
$preselectLimit = $modx->getOption(\'preselectLimit\', $scriptProperties, 0); // when random preselect important images
$where = $modx->getOption(\'where\', $scriptProperties, \'\');
$where = !empty($where) ? $modx->fromJSON($where) : array();
$sort = $modx->getOption(\'sort\', $scriptProperties, \'\');
$sort = !empty($sort) ? $modx->fromJSON($sort) : array();
$toSeparatePlaceholders = $modx->getOption(\'toSeparatePlaceholders\', $scriptProperties, false);
$toPlaceholder = $modx->getOption(\'toPlaceholder\', $scriptProperties, false);
$outputSeparator = $modx->getOption(\'outputSeparator\', $scriptProperties, \'\');
$splitSeparator = $modx->getOption(\'splitSeparator\', $scriptProperties, \'\');
$placeholdersKeyField = $modx->getOption(\'placeholdersKeyField\', $scriptProperties, \'MIGX_id\');
$toJsonPlaceholder = $modx->getOption(\'toJsonPlaceholder\', $scriptProperties, false);
$jsonVarKey = $modx->getOption(\'jsonVarKey\', $scriptProperties, \'migx_outputvalue\');
$outputvalue = $modx->getOption(\'value\', $scriptProperties, \'\');
$outputvalue = isset($_REQUEST[$jsonVarKey]) ? $_REQUEST[$jsonVarKey] : $outputvalue;
$docidVarKey = $modx->getOption(\'docidVarKey\', $scriptProperties, \'migx_docid\');
$docid = $modx->getOption(\'docid\', $scriptProperties, (isset($modx->resource) ? $modx->resource->get(\'id\') : 1));
$docid = isset($_REQUEST[$docidVarKey]) ? $_REQUEST[$docidVarKey] : $docid;
$processTVs = $modx->getOption(\'processTVs\', $scriptProperties, \'1\');
$reverse = $modx->getOption(\'reverse\', $scriptProperties, \'0\');
$sumFields = $modx->getOption(\'sumFields\', $scriptProperties, \'\');
$sumPrefix = $modx->getOption(\'sumPrefix\', $scriptProperties, \'summary_\');
$addfields = $modx->getOption(\'addfields\', $scriptProperties, \'\');
$addfields = !empty($addfields) ? explode(\',\', $addfields) : null;
//split json into parts
$splits = $modx->fromJson($modx->getOption(\'splits\', $scriptProperties, 0));
$splitTpl = $modx->getOption(\'splitTpl\', $scriptProperties, \'\');
$splitSeparator = $modx->getOption(\'splitSeparator\', $scriptProperties, \'\');
$inheritFrom = $modx->getOption(\'inheritFrom\', $scriptProperties, \'\'); //commaseparated list of resource-ids or/and the keyword \'parents\' where to inherit from
$inheritFrom = !empty($inheritFrom) ? explode(\',\',$inheritFrom) : \'\';

$modx->setPlaceholder(\'docid\', $docid);

$base_path = $modx->getOption(\'base_path\', null, MODX_BASE_PATH);
$base_url = $modx->getOption(\'base_url\', null, MODX_BASE_URL);

$migx = $modx->getService(\'migx\', \'Migx\', $modx->getOption(\'migx.core_path\', null, $modx->getOption(\'core_path\') . \'components/migx/\') . \'model/migx/\', $scriptProperties);
if (!($migx instanceof Migx))
    return \'\';
$migx->working_context = isset($modx->resource) ? $modx->resource->get(\'context_key\') : \'web\';

if (!empty($tvname)) {
    if ($tv = $modx->getObject(\'modTemplateVar\', array(\'name\' => $tvname))) {

        /*
        *   get inputProperties
        */


        $properties = $tv->get(\'input_properties\');
        $properties = isset($properties[\'formtabs\']) ? $properties : $tv->getProperties();

        $migx->config[\'configs\'] = $modx->getOption(\'configs\', $properties, \'\');
        if (!empty($migx->config[\'configs\'])) {
            $migx->loadConfigs();
            // get tabs from file or migx-config-table
            $formtabs = $migx->getTabs();
        }
        if (empty($formtabs) && isset($properties[\'formtabs\'])) {
            //try to get formtabs and its fields from properties
            $formtabs = $modx->fromJSON($properties[\'formtabs\']);
        }

        if (!empty($properties[\'basePath\'])) {
            if ($properties[\'autoResourceFolders\'] == \'true\') {
                $scriptProperties[\'base_path\'] = $base_path . $properties[\'basePath\'] . $docid . \'/\';
                $scriptProperties[\'base_url\'] = $base_url . $properties[\'basePath\'] . $docid . \'/\';
            } else {
                $scriptProperties[\'base_path\'] = $base_path . $properties[\'base_path\'];
                $scriptProperties[\'base_url\'] = $base_url . $properties[\'basePath\'];
            }
        }
        if ($jsonVarKey == \'migx_outputvalue\' && !empty($properties[\'jsonvarkey\'])) {
            $jsonVarKey = $properties[\'jsonvarkey\'];
            $outputvalue = isset($_REQUEST[$jsonVarKey]) ? $_REQUEST[$jsonVarKey] : $outputvalue;
        }
        
        if (empty($outputvalue)){
            $outputvalue = $tv->renderOutput($docid);
            if (empty($outputvalue) && !empty($inheritFrom)){
                foreach ($inheritFrom as $from){
                    if ($from == \'parents\'){
                        $outputvalue = $tv->processInheritBinding(\'\',$docid);
                    }else{
                        $outputvalue = $tv->renderOutput($from);
                    }
                    if (!empty($outputvalue)){
                        break;
                    }                    
                }
            }
        }
        
        
        /*
        *   get inputTvs 
        */
        $inputTvs = array();
        if (is_array($formtabs)) {

            //multiple different Forms
            // Note: use same field-names and inputTVs in all forms
            $inputTvs = $migx->extractInputTvs($formtabs);
        }
        if ($migx->source = $tv->getSource($migx->working_context, false)){
            $migx->source->initialize();
        }
        
    }


}

if (empty($outputvalue)) {
    return \'\';
}

//echo $outputvalue.\'<br/><br/>\';

$items = $modx->fromJSON($outputvalue);

// where filter
if (is_array($where) && count($where) > 0) {
    $items = $migx->filterItems($where, $items);
}
$modx->setPlaceholder($totalVar, count($items));

if (!empty($reverse)) {
    $items = array_reverse($items);
}

// sort items
if (is_array($sort) && count($sort) > 0) {
    $items = $migx->sortDbResult($items, $sort);
}

$summaries = array();
$output = \'\';
$items = $offset > 0 ? array_slice($items, $offset) : $items;
$count = count($items);

if ($count > 0) {
    $limit = $limit == 0 || $limit > $count ? $count : $limit;
    $preselectLimit = $preselectLimit > $count ? $count : $preselectLimit;
    //preselect important items
    $preitems = array();
    if ($randomize && $preselectLimit > 0) {
        for ($i = 0; $i < $preselectLimit; $i++) {
            $preitems[] = $items[$i];
            unset($items[$i]);
        }
        $limit = $limit - count($preitems);
    }

    //shuffle items
    if ($randomize) {
        shuffle($items);
    }

    //limit items
    $count = count($items);
    $tempitems = array();

    for ($i = 0; $i < $limit; $i++) {
        if ($i >= $count) {
            break;
        }
        $tempitems[] = $items[$i];
    }
    $items = $tempitems;

    //add preselected items and schuffle again
    if ($randomize && $preselectLimit > 0) {
        $items = array_merge($preitems, $items);
        shuffle($items);
    }

    $properties = array();
    foreach ($scriptProperties as $property => $value) {
        $properties[\'property.\' . $property] = $value;
    }

    $idx = 0;
    $output = array();
    $template = array();
    $count = count($items);

    foreach ($items as $key => $item) {
        $formname = isset($item[\'MIGX_formname\']) ? $item[\'MIGX_formname\'] . \'_\' : \'\';
        $fields = array();
        foreach ($item as $field => $value) {
            if (is_array($value)) {
                if (is_array($value[0])) {
                    //nested array - convert to json
                    $value = $modx->toJson($value);
                } else {
                    $value = implode(\'||\', $value); //handle arrays (checkboxes, multiselects)
                }
            }


            $inputTVkey = $formname . $field;
            if ($processTVs && isset($inputTvs[$inputTVkey])) {
                if (isset($inputTvs[$inputTVkey][\'inputTV\']) && $tv = $modx->getObject(\'modTemplateVar\', array(\'name\' => $inputTvs[$inputTVkey][\'inputTV\']))) {

                } else {
                    $tv = $modx->newObject(\'modTemplateVar\');
                    $tv->set(\'type\', $inputTvs[$inputTVkey][\'inputTVtype\']);
                }
                $inputTV = $inputTvs[$inputTVkey];

                $mTypes = $modx->getOption(\'manipulatable_url_tv_output_types\', null, \'image,file\');
                //don\'t manipulate any urls here
                $modx->setOption(\'manipulatable_url_tv_output_types\', \'\');
                $tv->set(\'default_text\', $value);
                $value = $tv->renderOutput($docid);
                //set option back
                $modx->setOption(\'manipulatable_url_tv_output_types\', $mTypes);
                //now manipulate urls
                if ($mediasource = $migx->getFieldSource($inputTV, $tv)) {
                    $mTypes = explode(\',\', $mTypes);
                    if (!empty($value) && in_array($tv->get(\'type\'), $mTypes)) {
                        //$value = $mediasource->prepareOutputUrl($value);
                        $value = str_replace(\'/./\', \'/\', $mediasource->prepareOutputUrl($value));
                    }
                }

            }
            $fields[$field] = $value;

        }

        if (!empty($addfields)) {
            foreach ($addfields as $addfield) {
                $addfield = explode(\':\', $addfield);
                $addname = $addfield[0];
                $adddefault = isset($addfield[1]) ? $addfield[1] : \'\';
                $fields[$addname] = $adddefault;
            }
        }

        if (!empty($sumFields)) {
            $sumFields = is_array($sumFields) ? $sumFields : explode(\',\', $sumFields);
            foreach ($sumFields as $sumField) {
                if (isset($fields[$sumField])) {
                    $summaries[$sumPrefix . $sumField] = $summaries[$sumPrefix . $sumField] + $fields[$sumField];
                    $fields[$sumPrefix . $sumField] = $summaries[$sumPrefix . $sumField];
                }
            }
        }


        if ($toJsonPlaceholder) {
            $output[] = $fields;
        } else {
            $fields[\'_alt\'] = $idx % 2;
            $idx++;
            $fields[\'_first\'] = $idx == 1 ? true : \'\';
            $fields[\'_last\'] = $idx == $limit ? true : \'\';
            $fields[\'idx\'] = $idx;
            $rowtpl = \'\';
            //get changing tpls from field
            if (substr($tpl, 0, 7) == "@FIELD:") {
                $tplField = substr($tpl, 7);
                $rowtpl = $fields[$tplField];
            }

            if ($fields[\'_first\'] && !empty($tplFirst)) {
                $rowtpl = $tplFirst;
            }
            if ($fields[\'_last\'] && empty($rowtpl) && !empty($tplLast)) {
                $rowtpl = $tplLast;
            }
            $tplidx = \'tpl_\' . $idx;
            if (empty($rowtpl) && !empty($$tplidx)) {
                $rowtpl = $$tplidx;
            }
            if ($idx > 1 && empty($rowtpl)) {
                $divisors = $migx->getDivisors($idx);
                if (!empty($divisors)) {
                    foreach ($divisors as $divisor) {
                        $tplnth = \'tpl_n\' . $divisor;
                        if (!empty($$tplnth)) {
                            $rowtpl = $$tplnth;
                            if (!empty($rowtpl)) {
                                break;
                            }
                        }
                    }
                }
            }

            if ($count == 1 && isset($tpl_oneresult)) {
                $rowtpl = $tpl_oneresult;
            }

            $fields = array_merge($fields, $properties);

            if (!empty($rowtpl)) {
                $template = $migx->getTemplate($tpl, $template);
                $fields[\'_tpl\'] = $template[$tpl];
            } else {
                $rowtpl = $tpl;

            }
            $template = $migx->getTemplate($rowtpl, $template);


            if ($template[$rowtpl]) {
                $chunk = $modx->newObject(\'modChunk\');
                $chunk->setCacheable(false);
                $chunk->setContent($template[$rowtpl]);


                if (!empty($placeholdersKeyField) && isset($fields[$placeholdersKeyField])) {
                    $output[$fields[$placeholdersKeyField]] = $chunk->process($fields);
                } else {
                    $output[] = $chunk->process($fields);
                }
            } else {
                if (!empty($placeholdersKeyField)) {
                    $output[$fields[$placeholdersKeyField]] = \'<pre>\' . print_r($fields, 1) . \'</pre>\';
                } else {
                    $output[] = \'<pre>\' . print_r($fields, 1) . \'</pre>\';
                }
            }
        }


    }
}

if (count($summaries) > 0) {
    $modx->toPlaceholders($summaries);
}


if ($toJsonPlaceholder) {
    $modx->setPlaceholder($toJsonPlaceholder, $modx->toJson($output));
    return \'\';
}

if (!empty($toSeparatePlaceholders)) {
    $modx->toPlaceholders($output, $toSeparatePlaceholders);
    return \'\';
}
/*
if (!empty($outerTpl))
$o = parseTpl($outerTpl, array(\'output\'=>implode($outputSeparator, $output)));
else 
*/

if ($count > 0 && $splits > 0) {
    $size = ceil($count / $splits);
    $chunks = array_chunk($output, $size);
    $output = array();
    foreach ($chunks as $chunk) {
        $o = implode($outputSeparator, $chunk);
        $output[] = $modx->getChunk($splitTpl, array(\'output\' => $o));
    }
    $outputSeparator = $splitSeparator;
}

if (is_array($output)) {
    $o = implode($outputSeparator, $output);
} else {
    $o = $output;
}

if (!empty($o) && !empty($wrapperTpl)) {
    $template = $migx->getTemplate($wrapperTpl);
    if ($template[$wrapperTpl]) {
        $chunk = $modx->newObject(\'modChunk\');
        $chunk->setCacheable(false);
        $chunk->setContent($template[$wrapperTpl]);
        $properties[\'output\'] = $o;
        $o = $chunk->process($properties);
    }
}

if (!empty($toPlaceholder)) {
    $modx->setPlaceholder($toPlaceholder, $o);
    return \'\';
}

return $o;',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
        ),
      ),
      'getDate' => 
      array (
        'fields' => 
        array (
          'id' => 16,
          'source' => 0,
          'property_preprocess' => false,
          'name' => 'getDate',
          'description' => 'A simple timestamp retrieval Snippet for MODX Revolution.',
          'editor_type' => 0,
          'category' => 8,
          'cache_type' => 0,
          'snippet' => '/**
 * A simple timestamp retrieval Snippet for MODX Revolution.
 *
 * @author David Pede <dev@tasianmedia.com> <https://twitter.com/davepede>
 * @version 1.0.0-pl
 * @released November 22, 2013
 * @since November 22, 2013
 * @package getdate
 *
 * Copyright (C) 2013 David Pede. All rights reserved. <dev@tasianmedia.com>
 *
 * getDate is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 *
 * getDate is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * getDate; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 */

/* set default properties */
$offset = !empty($offset) ? $offset : \'\';

$output = \'\';

$output = strtotime("$offset");

return (string) $output;',
          'locked' => false,
          'properties' => 
          array (
            'offset' => 
            array (
              'name' => 'offset',
              'desc' => 'The period of time to add or subtract from the current timestamp. Use relative date/time formats that the strtotime() parser understands.',
              'type' => 'textfield',
              'options' => '',
              'value' => 'now',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'The period of time to add or subtract from the current timestamp. Use relative date/time formats that the strtotime() parser understands.',
              'area_trans' => '',
            ),
          ),
          'moduleguid' => '',
          'static' => false,
          'static_file' => '',
          'content' => '/**
 * A simple timestamp retrieval Snippet for MODX Revolution.
 *
 * @author David Pede <dev@tasianmedia.com> <https://twitter.com/davepede>
 * @version 1.0.0-pl
 * @released November 22, 2013
 * @since November 22, 2013
 * @package getdate
 *
 * Copyright (C) 2013 David Pede. All rights reserved. <dev@tasianmedia.com>
 *
 * getDate is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 *
 * getDate is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * getDate; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 */

/* set default properties */
$offset = !empty($offset) ? $offset : \'\';

$output = \'\';

$output = strtotime("$offset");

return (string) $output;',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
        ),
      ),
    ),
    'modTemplateVar' => 
    array (
      'background-image' => 
      array (
        'fields' => 
        array (
          'id' => 1,
          'source' => 2,
          'property_preprocess' => false,
          'type' => 'image',
          'name' => 'background-image',
          'caption' => 'Header Image',
          'description' => '',
          'editor_type' => 0,
          'category' => 3,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 2,
          'name' => 'Assets',
          'description' => 'SubAdmin Media Access',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
            'basePath' => 
            array (
              'name' => 'basePath',
              'desc' => 'prop_file.basePath_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'css/images/',
              'lexicon' => 'core:source',
            ),
            'baseUrl' => 
            array (
              'name' => 'baseUrl',
              'desc' => 'prop_file.baseUrl_desc',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'css/images/',
              'lexicon' => 'core:source',
            ),
          ),
          'is_stream' => true,
        ),
      ),
    ),
  ),
);