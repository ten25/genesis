<?php /* Smarty version Smarty-3.0.4, created on 2015-04-27 04:56:04
         compiled from "/Users/justin/Desktop/repos/genesis/core/components/migx/elements/tv/tinymce.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1399178630553da54453bcd9-79563500%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '63e3c55248a509a67b7b310aea0694731be3c16d' => 
    array (
      0 => '/Users/justin/Desktop/repos/genesis/core/components/migx/elements/tv/tinymce.tpl',
      1 => 1430102717,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1399178630553da54453bcd9-79563500',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_escape')) include '/Users/justin/Desktop/repos/genesis/core/model/smarty/plugins/modifier.escape.php';
?><textarea id="tv<?php echo $_smarty_tpl->getVariable('tv')->value->id;?>
" name="tv<?php echo $_smarty_tpl->getVariable('tv')->value->id;?>
" class="rtf-tinymcetv tv<?php echo $_smarty_tpl->getVariable('tv')->value->id;?>
" onchange="MODx.fireResourceFormChange();"><?php echo smarty_modifier_escape($_smarty_tpl->getVariable('tv')->value->get('value'));?>
</textarea>

<script type="text/javascript">

Ext.onReady(function() {
    
    MODx.makeDroppable(Ext.get('tv<?php echo $_smarty_tpl->getVariable('tv')->value->id;?>
'));
    var tvid = 'tv<?php echo $_smarty_tpl->getVariable('tv')->value->id;?>
';
    
    var field = (Ext.get('tv<?php echo $_smarty_tpl->getVariable('tv')->value->id;?>
'));
    
    field.onLoad = function(){
        //console.log('we load');
        if (typeof(Tiny) != 'undefined') {
		    var s={};
            if (Tiny.config){
                s = Tiny.config || {};
                delete s.assets_path;
                delete s.assets_url;
                delete s.core_path;
                delete s.css_path;
                delete s.editor;
                delete s.id;
                delete s.mode;
                delete s.path;
                s.cleanup_callback = "Tiny.onCleanup";
                var z = Ext.state.Manager.get(MODx.siteId + '-tiny');
                if (z !== false) {
                    delete s.elements;
                }			
		    }
			//s.mode = "specific_textareas";
            //s.editor_selector = "modx-richtext";
            			
            s.mode = "exact";
            s.elements = "tv<?php echo $_smarty_tpl->getVariable('tv')->value->id;?>
";              
 		    //s.language = "en";// de seems not to work at the moment
            tinyMCE.init(s);
            
		}
    };
        
    field.onHide = function(){
        //console.log('we hide');
        if (typeof(tinyMCE) != 'undefined') {
            var tinyinstance = tinyMCE.getInstanceById('tv<?php echo $_smarty_tpl->getVariable('tv')->value->id;?>
');
            if (typeof(tinyinstance) != 'undefined') {
                tinyinstance.remove();
            }
        }     
    };
        
    field.onBeforeSubmit = function(){
        //console.log('we submit');
        if (typeof(tinyMCE) != 'undefined') {
            tinyMCE.getInstanceById('tv<?php echo $_smarty_tpl->getVariable('tv')->value->id;?>
').save(); 
        }       
    };        


});

</script>
