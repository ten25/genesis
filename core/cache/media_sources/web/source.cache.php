<?php  return array (
  1 => 
  array (
    'basePath' => 'css/images/',
    'basePathRelative' => true,
    'baseUrl' => 'css/images/',
    'baseUrlRelative' => true,
    'allowedFileTypes' => '',
    'imageExtensions' => 'jpg,jpeg,png,gif',
    'thumbnailType' => 'png',
    'thumbnailQuality' => 90,
    'skipFiles' => '.svn,.git,_notes,nbproject,.idea,.DS_Store',
    'id' => NULL,
    'name' => 'Assets',
    'description' => 'SubAdmin Media Access',
    'class_key' => 'modFileMediaSource',
    'properties' => 
    array (
      'basePath' => 
      array (
        'name' => 'basePath',
        'desc' => 'prop_file.basePath_desc',
        'type' => 'textfield',
        'options' => 
        array (
        ),
        'value' => 'css/images/',
        'lexicon' => 'core:source',
      ),
      'baseUrl' => 
      array (
        'name' => 'baseUrl',
        'desc' => 'prop_file.baseUrl_desc',
        'type' => 'textfield',
        'options' => 
        array (
        ),
        'value' => 'css/images/',
        'lexicon' => 'core:source',
      ),
    ),
    'is_stream' => true,
    'source' => 2,
    'object_class' => 'modTemplateVar',
    'object' => 1,
    'context_key' => 'web',
    'source_class_key' => 'sources.modFileMediaSource',
  ),
  2 => 
  array (
    'basePath' => 'css/images/',
    'basePathRelative' => true,
    'baseUrl' => 'css/images/',
    'baseUrlRelative' => true,
    'allowedFileTypes' => '',
    'imageExtensions' => 'jpg,jpeg,png,gif',
    'thumbnailType' => 'png',
    'thumbnailQuality' => 90,
    'skipFiles' => '.svn,.git,_notes,nbproject,.idea,.DS_Store',
    'id' => NULL,
    'name' => 'Assets',
    'description' => 'SubAdmin Media Access',
    'class_key' => 'modFileMediaSource',
    'properties' => 
    array (
      'basePath' => 
      array (
        'name' => 'basePath',
        'desc' => 'prop_file.basePath_desc',
        'type' => 'textfield',
        'options' => 
        array (
        ),
        'value' => 'css/images/',
        'lexicon' => 'core:source',
      ),
      'baseUrl' => 
      array (
        'name' => 'baseUrl',
        'desc' => 'prop_file.baseUrl_desc',
        'type' => 'textfield',
        'options' => 
        array (
        ),
        'value' => 'css/images/',
        'lexicon' => 'core:source',
      ),
    ),
    'is_stream' => true,
    'source' => 2,
    'object_class' => 'modTemplateVar',
    'object' => 2,
    'context_key' => 'web',
    'source_class_key' => 'sources.modFileMediaSource',
  ),
  3 => 
  array (
    'basePath' => 'css/images/',
    'basePathRelative' => true,
    'baseUrl' => 'css/images/',
    'baseUrlRelative' => true,
    'allowedFileTypes' => '',
    'imageExtensions' => 'jpg,jpeg,png,gif',
    'thumbnailType' => 'png',
    'thumbnailQuality' => 90,
    'skipFiles' => '.svn,.git,_notes,nbproject,.idea,.DS_Store',
    'id' => NULL,
    'name' => 'Assets',
    'description' => 'SubAdmin Media Access',
    'class_key' => 'modFileMediaSource',
    'properties' => 
    array (
      'basePath' => 
      array (
        'name' => 'basePath',
        'desc' => 'prop_file.basePath_desc',
        'type' => 'textfield',
        'options' => 
        array (
        ),
        'value' => 'css/images/',
        'lexicon' => 'core:source',
      ),
      'baseUrl' => 
      array (
        'name' => 'baseUrl',
        'desc' => 'prop_file.baseUrl_desc',
        'type' => 'textfield',
        'options' => 
        array (
        ),
        'value' => 'css/images/',
        'lexicon' => 'core:source',
      ),
    ),
    'is_stream' => true,
    'source' => 2,
    'object_class' => 'modTemplateVar',
    'object' => 3,
    'context_key' => 'web',
    'source_class_key' => 'sources.modFileMediaSource',
  ),
  4 => 
  array (
    'basePath' => 'css/images/',
    'basePathRelative' => true,
    'baseUrl' => 'css/images/',
    'baseUrlRelative' => true,
    'allowedFileTypes' => '',
    'imageExtensions' => 'jpg,jpeg,png,gif',
    'thumbnailType' => 'png',
    'thumbnailQuality' => 90,
    'skipFiles' => '.svn,.git,_notes,nbproject,.idea,.DS_Store',
    'id' => NULL,
    'name' => 'Assets',
    'description' => 'SubAdmin Media Access',
    'class_key' => 'modFileMediaSource',
    'properties' => 
    array (
      'basePath' => 
      array (
        'name' => 'basePath',
        'desc' => 'prop_file.basePath_desc',
        'type' => 'textfield',
        'options' => 
        array (
        ),
        'value' => 'css/images/',
        'lexicon' => 'core:source',
      ),
      'baseUrl' => 
      array (
        'name' => 'baseUrl',
        'desc' => 'prop_file.baseUrl_desc',
        'type' => 'textfield',
        'options' => 
        array (
        ),
        'value' => 'css/images/',
        'lexicon' => 'core:source',
      ),
    ),
    'is_stream' => true,
    'source' => 2,
    'object_class' => 'modTemplateVar',
    'object' => 4,
    'context_key' => 'web',
    'source_class_key' => 'sources.modFileMediaSource',
  ),
  5 => 
  array (
    'basePath' => 'css/images/',
    'basePathRelative' => true,
    'baseUrl' => 'css/images/',
    'baseUrlRelative' => true,
    'allowedFileTypes' => '',
    'imageExtensions' => 'jpg,jpeg,png,gif',
    'thumbnailType' => 'png',
    'thumbnailQuality' => 90,
    'skipFiles' => '.svn,.git,_notes,nbproject,.idea,.DS_Store',
    'id' => NULL,
    'name' => 'Assets',
    'description' => 'SubAdmin Media Access',
    'class_key' => 'modFileMediaSource',
    'properties' => 
    array (
      'basePath' => 
      array (
        'name' => 'basePath',
        'desc' => 'prop_file.basePath_desc',
        'type' => 'textfield',
        'options' => 
        array (
        ),
        'value' => 'css/images/',
        'lexicon' => 'core:source',
      ),
      'baseUrl' => 
      array (
        'name' => 'baseUrl',
        'desc' => 'prop_file.baseUrl_desc',
        'type' => 'textfield',
        'options' => 
        array (
        ),
        'value' => 'css/images/',
        'lexicon' => 'core:source',
      ),
    ),
    'is_stream' => true,
    'source' => 2,
    'object_class' => 'modTemplateVar',
    'object' => 5,
    'context_key' => 'web',
    'source_class_key' => 'sources.modFileMediaSource',
  ),
);