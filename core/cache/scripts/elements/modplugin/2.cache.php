<?php  return '// CHANGE THESE AS NEEDED
$phpThumb_file_location = "resources/libs/phpthumb/ThumbLib.inc.php";

// MEDIA SOURCE FOR LOCKED DOWN IMAGE UPLOADING
$tv_media_source = \'assets/media/\';

// MODIFY THE ARRAY STRUCTURE BELOW FOR EACH TEMPLATE VARIABLE THAT NEEDS AUTORESIZING
$tv_array = array(
                  \'homepage_rotator_image\' => array(
                    \'large_size\' => \'1024x577\',
                    \'large_crop\' => 1,
                    \'retina\' => 1,
                  )
                );

$tv_names = implode("\',\'",array_keys($tv_array));

//GET THE TVS FROM THE DB
$sql = "SELECT A.*";
$sql .= " FROM modx_site_tmplvars A";
$sql .= " WHERE A.type = \'image\' AND A.name IN (\'" . $tv_names . "\')";

// GET VALUES FROM DATABASE AND SET PARAMS
$result = $modx->query($sql);
if ($result && $result instanceof PDOStatement) {
  $image_array = array();
  $image_settings_array = array();
  while ($gal_var = $result->fetch(PDO::FETCH_ASSOC)) {

    if (isset($_REQUEST[\'tv\'.$gal_var[\'id\']])) {
      // BUILD THE ARRAY OF IMAGES THAT WERE FOUND IN THE DOCUMENT FOR PROCESSING
      $image_array[] = $_REQUEST[\'tv\'.$gal_var[\'id\']];
      $image_settings_array[] = $tv_array[$gal_var[\'name\']];
    }

  }
}

if(!empty($image_array)) {

  foreach ($image_array as $key=>$upload_file) {

    $modx->log(modX::LOG_LEVEL_ERROR,\'Starting \'.$upload_file);

    set_time_limit(300);

    if(is_null($upload_file))
      return false;

    include_once ($modx->getOption(\'base_path\') . $phpThumb_file_location);

    $full_path = $modx->getOption(\'base_path\') . $upload_file;
    if(!is_file($full_path)) {
      $full_path = $modx->getOption(\'base_path\') . $tv_media_source . $upload_file;
    }
    if(!is_file($full_path)) {
      break;
    }
    $file_name = basename($full_path);
    $image_directory = dirname($full_path);

    // MAKE SOME LARGE ONES
    if(isset($image_settings_array[$key][\'large_size\'])) {
        try
        {
          $large = PhpThumbFactory::create($full_path);
        }
        catch (Exception $e)
        {
    // handle error here however you\'d like
        }

        $large_size_array = explode("x",$image_settings_array[$key][\'large_size\']);

        if ($image_settings_array[$key][\'retina\']) {
          if ($image_settings_array[$key][\'large_crop\']) {
            $large->adaptiveResize($large_size_array[0]*2, $large_size_array[1]*2);
          } else {
            $large->resize($large_size_array[0]*2, $large_size_array[1]*2);
          }
          $extension_pos = strrpos($file_name, \'.\');
          $file_name_amended = substr($file_name, 0, $extension_pos) . \'_large@2x\' . substr($file_name, $extension_pos);
          $large->save($image_directory . \'/\' . $file_name_amended );
        }

        if ($image_settings_array[$key][\'large_crop\']) {
          $large->adaptiveResize($large_size_array[0], $large_size_array[1]);
        } else {
          $large->resize($large_size_array[0], $large_size_array[1]);
        }
        $extension_pos = strrpos($file_name, \'.\');
        $file_name_amended = substr($file_name, 0, $extension_pos) . \'_large\' . substr($file_name, $extension_pos);
        $large->save($image_directory . \'/\' . $file_name_amended );
    }


    $modx->log(modX::LOG_LEVEL_ERROR,\'Finished \'.$upload_file);

  }
}
return;
';