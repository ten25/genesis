<?php
/**
 * Default English Lexicon Entries for ContactSubmissions
 *
 * @package contactsubmissions
 * @subpackage lexicon
 */
$_lang['contactsubmission'] = 'Contact Submission';
$_lang['contactsubmissions'] = 'Contact Submissions';
$_lang['contactsubmissions.desc'] = 'Manage your contact submissions here.';
$_lang['contactsubmissions.description'] = 'Description';
$_lang['contactsubmissions.contactsubmission_err_ae'] = 'A contact with that email already exists.';
$_lang['contactsubmissions.contactsubmission_err_nf'] = 'Contact not found.';
$_lang['contactsubmissions.contactsubmission_err_ns'] = 'Contact not specified.';
$_lang['contactsubmissions.contactsubmission_err_ns_name'] = 'Please specify a name for the contact.';
$_lang['contactsubmissions.contactsubmission_err_remove'] = 'An error occurred while trying to remove the contact.';
$_lang['contactsubmissions.contactsubmission_err_save'] = 'An error occurred while trying to save the contact.';
$_lang['contactsubmissions.contactsubmission_err_data'] = 'Invalid data.';
$_lang['contactsubmissions.contactsubmission_create'] = 'Create New Contact';
$_lang['contactsubmissions.contactsubmission_remove'] = 'Remove Contact';
$_lang['contactsubmissions.contactsubmission_remove_confirm'] = 'Are you sure you want to remove this contact?';
$_lang['contactsubmissions.contactsubmission_update'] = 'Update Contact';
$_lang['contactsubmissions.downloads'] = 'Downloads';
$_lang['contactsubmissions.location'] = 'Location';
$_lang['contactsubmissions.management'] = 'Contact Management';
$_lang['contactsubmissions.management_desc'] = 'Manage your contacts here. You can edit them by either double-clicking on the grid or right-clicking on the respective row.';
$_lang['contactsubmissions.id'] = 'ID';
$_lang['contactsubmissions.name'] = 'Name';
$_lang['contactsubmissions.email'] = 'Email';
$_lang['contactsubmissions.company'] = 'Company';
$_lang['contactsubmissions.phone'] = 'Phone';
$_lang['contactsubmissions.message'] = 'Message';
$_lang['contactsubmissions.createdon'] = 'Created On';
$_lang['contactsubmissions.search...'] = 'Search...';
$_lang['contactsubmissions.top_downloaded'] = 'Top Downloaded Contacts';

